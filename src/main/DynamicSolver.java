package main;

import model.problem.*;
import model.solution.*;
import solver.*;
import solver.moves.assignmentmoves.*;
import solver.search.*;

import java.util.*;

public class DynamicSolver implements Solver<PDPTWSolution> {
  public final Random random;
  public static double previousCost = Double.POSITIVE_INFINITY;
//  private final Semaphore newSnapshot;

  public DynamicSolver(Random random) {
    this.random = random;
//    this.newSnapshot = newSnapshot;
  }

  @Override
  public PDPTWSolution solve(PDPTWSolution solution) throws InterruptedException {
    System.out.println("begin dynamic solver "+solution.calculateCost());
    int i = 0;
    double c = solution.calculateCost();
    System.out.println(solution.toString());
    System.out.println(c+" "+previousCost);
    System.out.println(c);
    for (final Request request : solution.problem.getRequests()) {
      if(Thread.interrupted())throw new InterruptedException();
      System.out.println("----- new request: " + i+" "+request.arrivalTime+" "+request.pickup.timeWindow.startTime+" "+request.pickup.timeWindow.endTime+" "+request.delivery.timeWindow.startTime+" "+request.delivery.timeWindow.endTime);


      if(request.pickup.timeWindow.endTime < request.arrivalTime + 1800){
        System.out.println("Cannot handle request, called in too late");
        i++;
        continue;
      }
      solution.problem.currentTime = request.arrivalTime;

      System.out.println("steepest descent");
      final LocalSearch<PDPTWSolution> localSearch =
        new SteepestDescent<>(solution, new PutMoveFactory(request),
            solution.calculateCost(), random);
      localSearch.performIteration();
//      System.out.println(solution.calculateCost());
      previousCost = solution.calculateCost();

      System.out.println("local search");
      final Solver<PDPTWSolution> solver = new LocalSearchSolver(random);
      // System.out.println((nextTime - currentTime)*PDPTWMain.timeFactor+"
      // "+(nextTime - currentTime)+" "+PDPTWMain.timeFactor);
      solution = solver.solve(solution);
      i++;
      System.out.println(i);
      System.out.println(solution.toString());
    }
    System.out.println("dynamic solver done");
    double cost = solution.calculateCost();
    System.out.println(cost+" : "+solution.calculateTravelTime()+" "+solution.calculateLateness()+" "+solution.calculateOvertime());

    previousCost = cost;
    System.out.println(solution.toString());
    return solution;
  }
}
