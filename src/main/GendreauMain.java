package main;

import model.problem.PDPTWProblem;
import model.solution.PDPTWSolution;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Eline on 27/11/2015.
 */
public class GendreauMain {

    public static void main(String[] args) throws IOException, InterruptedException {
        String path = args[0];
        String solutionPath = path+"_solution";
        File file = new File(solutionPath);
        PDPTWProblem problem = new PDPTWProblem(path);
        ElineSolver solver = new ElineSolver(206720876827L);
        PDPTWSolution solution = solver.solve(problem);
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        bufferedWriter.write(solution.writeToFile());
        bufferedWriter.flush();
        bufferedWriter.close();
    }

}
