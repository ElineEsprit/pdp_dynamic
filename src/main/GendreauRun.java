package main;

import model.problem.PDPTWProblem;
import model.solution.PDPTWSolution;

import java.io.*;
import java.util.ArrayList;

public class GendreauRun {

    public static void main(String[] args) throws IOException, InterruptedException {
        ArrayList<String> list = new ArrayList<String>();
        //list.add("files/req_rapide_0_240_24");
        //list.add("files/req_rapide_0_240_24");
        //list.add("files/req_rapide_0_240_24");
        //list.add("files/req_rapide_0_240_24");
//        list.add("files/req_rapide_0_240_24");
        list.add("files/req_rapide_1_240_24");
//        list.add("files/req_rapide_2_240_24");
        //list.add("files/req_rapide_3_240_24");
        //list.add("files/req_rapide_4_240_24");
        //list.add("files/req_rapide_5_240_24");
        //list.add("files/req_rapide_1_240_33");
        //list.add("files/req_rapide_2_240_33");
        //list.add("files/req_rapide_3_240_33");
        //list.add("files/req_rapide_4_240_33");
        //list.add("files/req_rapide_5_240_33");
        //list.add("files/req_rapide_1_450_24");
        //list.add("files/req_rapide_2_450_24");
        //list.add("files/req_rapide_3_450_24");
        //list.add("files/req_rapide_4_450_24");
        //list.add("files/req_rapide_5_450_24");

        File file = new File("results.csv");

        for (String path : list) {
            PDPTWProblem problem = new PDPTWProblem(path);
            ElineSolver solver = new ElineSolver(206720876827L);
            PDPTWSolution solution = solver.solve(problem);
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));
            bufferedWriter.write(path+" "+solution.calculateCost()+" : "+solution.calculateTravelTime()+" "+solution.calculateLateness()+" "+solution.calculateOvertime()+"\n");
            bufferedWriter.flush();
            bufferedWriter.close();
        }
    }
}
