package main;//package main;
//
//import com.github.rinde.rinsim.central.Central;
//import com.github.rinde.rinsim.central.SolverValidator;
//import com.github.rinde.rinsim.core.model.time.TimeModel;
//import com.github.rinde.rinsim.experiment.Experiment;
//import com.github.rinde.rinsim.experiment.ExperimentResults;
//import com.github.rinde.rinsim.experiment.PostProcessors;
//import com.github.rinde.rinsim.pdptw.common.StatisticsDTO;
//import com.github.rinde.rinsim.scenario.Scenario;
//import com.github.rinde.rinsim.scenario.gendreau06.Gendreau06ObjectiveFunction;
//import com.github.rinde.rinsim.scenario.gendreau06.Gendreau06Parser;
//import com.github.rinde.rinsim.scenario.gendreau06.Gendreau06Scenario;
//
//import javax.measure.unit.SI;
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.ArrayList;
//
//public class GendreauRunNrt {
//
//    public static void main(String[] args){
//        ArrayList<String> list = new ArrayList<String>();
//        //list.add("files/req_rapide_0_240_24");
//        //list.add("files/req_rapide_0_240_24");
//        //list.add("files/req_rapide_0_240_24");
//        //list.add("files/req_rapide_0_240_24");
////        list.add("files/req_rapide_0_240_24");
//        list.add("files/req_rapide_1_240_24");
////        list.add("files/req_rapide_2_240_24");
////        list.add("files/req_rapide_3_240_24");
////        list.add("files/req_rapide_4_240_24");
////        list.add("files/req_rapide_5_240_24");
////        list.add("files/req_rapide_1_240_33");
////        list.add("files/req_rapide_2_240_33");
////        list.add("files/req_rapide_3_240_33");
////        list.add("files/req_rapide_4_240_33");
////        list.add("files/req_rapide_5_240_33");
////        list.add("files/req_rapide_1_450_24");
////        list.add("files/req_rapide_2_450_24");
////        list.add("files/req_rapide_3_450_24");
////        list.add("files/req_rapide_4_450_24");
////        list.add("files/req_rapide_5_450_24");
//
//        for (String path : list) {
//            final Gendreau06Scenario scenario =
//              Gendreau06Parser.parse(new File(path));
//
//            // converts the scenario to real time
//            final Scenario rtScenario = Scenario.builder(scenario)
//              .removeModelsOfType(TimeModel.Builder.class)
//              .addModel(TimeModel.builder()
////                .withRealTime()
//                .withTickLength(1000L)
//                .withTimeUnit(SI.MILLI(SI.SECOND))
//                /*.withStartInClockMode(RealtimeClockController.ClockMode.SIMULATED)*/)
//              .build();
//
//            final ExperimentResults res =
//              Experiment.build(Gendreau06ObjectiveFunction.instance())
//                .addScenario(rtScenario)
//                .addConfiguration(Central.solverConfiguration(
//                  SolverValidator.wrap(ElineSolver.supplier()), "ElineSolver"))
//                  //.showGui(View.builder()
//                  //  .with(RoadUserRenderer.builder())
//                  //  .with(PlaneRoadModelRenderer.builder())
//                  //  .with(PDPModelRenderer.builder())
//                  //  .with(TimeLinePanel.builder())
//                  //  .withAutoPlay())
//                .withThreads(1)
//                .usePostProcessor(PostProcessors.statisticsPostProcessor())
//                .perform();
//
//            final StatisticsDTO stats =
//              (StatisticsDTO) res.getResults().asList().get(0).getResultObject();
//
//            File file = new File("results.csv");
//            try {
//                System.out.println(path + " " + Gendreau06ObjectiveFunction.instance().computeCost(stats));
//
//                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file,true));
//
//                bufferedWriter.write(path + " " + Gendreau06ObjectiveFunction.instance().computeCost(stats) + "\n");
//                bufferedWriter.flush();
//                bufferedWriter.close();
//            }
//            catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//}
