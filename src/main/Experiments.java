package main;//package main;
//
//import com.github.rinde.rinsim.central.rt.*;
//import com.github.rinde.rinsim.core.model.time.*;
//import com.github.rinde.rinsim.experiment.*;
//import com.github.rinde.rinsim.pdptw.common.*;
//import com.github.rinde.rinsim.scenario.*;
//import com.github.rinde.rinsim.scenario.gendreau06.*;
//
//import javax.measure.unit.*;
//import java.io.*;
//
//public class Experiments {
//
//    public static void main(String[] args){
//
//        final Gendreau06Scenario scenario =
//          Gendreau06Parser.parse(new File(args[0]));
//
//        // converts the scenario to real time
//        final Scenario rtScenario = Scenario.builder(scenario)
//          .removeModelsOfType(TimeModel.Builder.class)
//          .addModel(TimeModel.builder()
//            .withRealTime()
//            .withTickLength(1000L)
//            .withTimeUnit(SI.MILLI(SI.SECOND))
//            .withStartInClockMode(RealtimeClockController.ClockMode.SIMULATED))
//          .build();
//
//        final ExperimentResults res =
//          Experiment.build(Gendreau06ObjectiveFunction.instance())
//            .addScenario(rtScenario)
//            .addConfiguration(RtCentral.solverConfiguration(
//              RtElineSolver.supplier(), "ElineSolver"))
//              //.showGui(View.builder()
//              //    .with(RoadUserRenderer.builder())
//              //    .with(PlaneRoadModelRenderer.builder())
//              //    .with(PDPModelRenderer.builder())
//              //    .with(TimeLinePanel.builder())
//              //    .withAutoPlay())
//            .withThreads(1)
//            .usePostProcessor(PostProcessors.statisticsPostProcessor())
//            .perform();
//
//        final StatisticsDTO stats =
//          (StatisticsDTO) res.getResults().asList().get(0).getResultObject();
//
//        //System.out.println("Gendreau cost");
//        File theDir = new File("elineSolutions");
//        if(!theDir.exists())theDir.mkdir();
//        File file = new File("elineSolutions/"+args[0].substring(args[0].lastIndexOf("/")+1)+"_solution");
//        try {
//            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
//            bufferedWriter.write(args[0] + " " + Gendreau06ObjectiveFunction.instance().computeCost(stats) + "\n");
//            bufferedWriter.flush();
//            bufferedWriter.close();
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//}
