package main;

import model.problem.PDPTWProblem;
import model.solution.PDPTWSolution;

import java.util.Random;

public class ElineSolver{
  private final long randomSeed;
  public final Random random;
  public static int i=0;

  ElineSolver(long seed) {
    randomSeed = seed;
    random = new Random(randomSeed);
  }

  public PDPTWSolution solve(PDPTWProblem problem) throws InterruptedException {
    final PDPTWSolution solution = PDPTWMain.processOne(problem, random);
//    System.out.println("new schedule");
//    System.out.println("converted");
//    System.out.println(Joiner.on("\n").join(solution.routes));
    System.out.println(solution.calculateCost());

    return solution;
  }


}
