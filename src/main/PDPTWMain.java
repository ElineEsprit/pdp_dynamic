package main;

import model.problem.PDPTWProblem;
import model.solution.PDPTWSolution;
import solver.Solver;

import java.util.Random;

public class PDPTWMain {

  public static final int NON_IMPROVEMENT = 100000;
//  public static PDPTWSolution toVisualize;
//  public static Solver<Route2> routeSolver;
//  public static final double timeFactor = (double) 1 / (double) 1;

  // public static void main(String[] args) throws IOException {
  // final BufferedWriter bufferedWriter =
  // new BufferedWriter(new FileWriter(new File("files/log"), true));
  // for (final String path : args) {
  // bufferedWriter.write(processOne(path) + "\n");
  // bufferedWriter.flush();
  // }
  // bufferedWriter.close();
  // }

  // public static String processOne(String path) {
  // System.out.println(path);
  // routeSolver = new SimpleRouteSolver(0);
  // PDPTWProblem problem = null;
  // try {
  // problem = new PDPTWProblem(path);
  // } catch (final IOException e) {
  // e.printStackTrace();
  // }
  //
  // final PDPTWSolution solution = processOne(problem);
  // toVisualize = solution;
  // System.out.println(path + " " + solution.printCost());
  //
  // return path + " " + solution.printCost();
  // }

  public static PDPTWSolution processOne(PDPTWProblem problem, Random random) throws InterruptedException {
    PDPTWSolution solution = new PDPTWSolution(problem);

    final Solver<PDPTWSolution> solver = new DynamicSolver(random);
    solution = solver.solve(solution);

    return solution;
  }

}