package main;//package main;
//
//import com.github.rinde.rinsim.central.GlobalStateObject;
//import com.github.rinde.rinsim.central.Solvers;
//import com.github.rinde.rinsim.central.rt.RealtimeSolver;
//import com.github.rinde.rinsim.central.rt.Scheduler;
//import com.github.rinde.rinsim.core.model.pdp.Parcel;
//import com.github.rinde.rinsim.pdptw.common.StatisticsDTO;
//import com.github.rinde.rinsim.scenario.gendreau06.Gendreau06ObjectiveFunction;
//import com.github.rinde.rinsim.util.StochasticSupplier;
//import com.google.common.base.Optional;
//import com.google.common.collect.ImmutableList;
//import com.google.common.util.concurrent.ListenableFuture;
//import model.problem.Destination;
//import model.problem.PDPTWProblem;
//import model.solution.PDPTWSolution;
//import model.solution.Route2;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.util.Random;
//
//import static com.google.common.base.Preconditions.checkArgument;
//import static com.google.common.base.Preconditions.checkState;
//
//public class RtElineSolver implements RealtimeSolver {
//  public static int interrupted = 0;
//  static final Logger LOGGER =
//          LoggerFactory.getLogger(RtElineSolver.class);
//
//  private final Random random;
//  Optional<Scheduler> scheduler;
//  Optional<ListenableFuture<Object>> currentFuture;
//  Optional<SolverComputer> solverComputer;
////  private final Semaphore newSnapshot;
//
//  public static int i = 0;
//
//
//  RtElineSolver(long seed) {
//    System.out.println(seed);
//    this.random = new Random(seed);
//    scheduler = Optional.absent();
//    currentFuture = Optional.absent();
//    solverComputer = Optional.absent();
////    newSnapshot = new Semaphore(0);
//  }
//
//  @Override
//  public void init(Scheduler scheduler) {
//    this.scheduler = Optional.of(scheduler);
//  }
//
//  @Override
//  public void receiveSnapshot(GlobalStateObject snapshot) {
//    // TODO update solver with latest info
//
//    if(currentFuture.isPresent() && solverComputer.isPresent() ){
//
//      if(!printSchedule(solverComputer.get().snapshot, solverComputer.get().scheduler.getCurrentSchedule()).equals(printSnapshot(snapshot))){
//
//
////        System.out.println(printSchedule(solverComputer.get().snapshot, solverComputer.get().scheduler.getCurrentSchedule()));
////        System.out.println(printSnapshot(snapshot));
//        i--;
//        problemChanged(snapshot);
//
//      }
////      else if(newSnapshot.availablePermits()==0 && newSnapshot.getQueueLength()>0){
////        newSnapshot.release();
////      }
//    }
//  }
//
//  @Override
//  public void problemChanged(GlobalStateObject snapshot) {
//    i++;
//    System.out.println("----- New snapshot " + i);
//    checkArgument(snapshot.getVehicles().get(0).getRoute().isPresent(),
//            "We expect existing routes.");
//
//    checkState(scheduler.isPresent(), "Not yet initialized.");
//    if (currentFuture.isPresent() && !currentFuture.get().isDone()) {
//      LOGGER.trace("attempt to cancel running Solver..");
//      currentFuture.get().cancel(true);
//      interrupted++;
//    }
//
////    newSnapshot.drainPermits();
//    System.out.println("***** " + interrupted);
//    SolverComputer sc = new SolverComputer(random, scheduler.get(), snapshot/*, newSnapshot*/);
//    final ListenableFuture<Object> f =
//            (ListenableFuture<Object>) scheduler.get().getSharedExecutor().submit(sc);
//
//    solverComputer = Optional.of(sc);
//    currentFuture = Optional.of(f);
//  }
//
//  @Override
//  public void cancel() {
//    if (currentFuture.isPresent()) {
//      currentFuture.get().cancel(true);
//    }
//  }
//
//  @Override
//  public boolean isComputing() {
//    return currentFuture.isPresent() && !currentFuture.get().isDone();
//  }
//
//  public static StochasticSupplier<RealtimeSolver> supplier() {
//    return ElineSolverSupplier.INSTANCE;
//  }
//
//  enum ElineSolverSupplier
//          implements StochasticSupplier<RealtimeSolver> {
//    INSTANCE {
//      @Override
//      public RealtimeSolver get(long seed) {
//        return new RtElineSolver(seed);
//      }
//    }
//  }
//
//  public static Parcel dest2Parcel(Destination dest) {
//    return dest.request.getParcel();
//  }
//
//  public static ImmutableList<ImmutableList<Parcel>> solution2Schedule(
//          PDPTWSolution sol) {
//    final ImmutableList.Builder<ImmutableList<Parcel>> scheduleBuilder =
//            ImmutableList.builder();
//
//    for (final Route2 r : sol.routes) {
//      final ImmutableList.Builder<Parcel> routeBuilder =
//              ImmutableList.builder();
//      for (final Destination dest : r.keySet()) {
//        routeBuilder.add(dest2Parcel(dest));
//      }
//      scheduleBuilder.add(routeBuilder.build());
//    }
//
//    final ImmutableList<ImmutableList<Parcel>> schedule =
//            scheduleBuilder.build();
//
//    return schedule;
//  }
//
//  public static ImmutableList<ImmutableList<Parcel>> snapshot2Schedule(
//          GlobalStateObject snapshot) {
//    final ImmutableList.Builder<ImmutableList<Parcel>> scheduleBuilder =
//            ImmutableList.builder();
//
//    for (final GlobalStateObject.VehicleStateObject vso : snapshot.getVehicles()) {
//      final ImmutableList.Builder<Parcel> routeBuilder =
//              ImmutableList.builder();
//      for (final Parcel parcel : vso.getRoute().get()) {
//        routeBuilder.add(parcel);
//      }
//      scheduleBuilder.add(routeBuilder.build());
//    }
//
//    final ImmutableList<ImmutableList<Parcel>> schedule =
//            scheduleBuilder.build();
//
//    return schedule;
//  }
//
//  public String printSnapshot(GlobalStateObject globalStateObject){
//    String line="";
//    for(GlobalStateObject.VehicleStateObject vehicleStateObject : globalStateObject.getVehicles()){
//      if (vehicleStateObject.getRoute().isPresent()) {
//        for(Parcel parcel : vehicleStateObject.getRoute().get()){
//          if(!vehicleStateObject.getDestination().isPresent()||!parcel.equals(vehicleStateObject.getDestination().get()))line = line+(parcel.toString()+" ");
//          else line = line+("*"+parcel.toString()+"* ");
//        }
//      }
//      line = line+"\n";
//    }
//    return line;
//  }
//
//  public String printSchedule(GlobalStateObject globalStateObject, ImmutableList<ImmutableList<Parcel>> schedule){
//    String line="";
//    for(int i=0;i<globalStateObject.getVehicles().size();i++){
//
//      if (globalStateObject.getVehicles().get(i).getRoute().isPresent() && schedule.get(i).size()>0) {
//        for(int j=0;j<schedule.get(i).size();j++){
//          Parcel parcel = schedule.get(i).get(j);
//          if(!globalStateObject.getVehicles().get(i).getDestination().isPresent()||!parcel.equals(globalStateObject.getVehicles().get(i).getDestination().get()))line = line+(parcel.toString()+" ");
//          else line = line+("*"+parcel.toString()+"* ");
//        }
//      }
//      line = line+"\n";
//    }
//    return line;
//  }
//
//  static class SolverComputer
//          implements Runnable {
//    final Random random;
//    final Scheduler scheduler;
//    final GlobalStateObject snapshot;
//
////    private final Semaphore newSnapshot;
//
//    SolverComputer(Random random, Scheduler scheduler, GlobalStateObject snap/*, Semaphore newSnapshot*/) {
//      this.random = random;
//      this.scheduler = scheduler;
//      snapshot = snap;
////      this.newSnapshot = newSnapshot;
//    }
//
//    @Override
//    public void run() {
//
//      final PDPTWProblem prob = new PDPTWProblem(snapshot);
//      try {
//        final PDPTWSolution sol = PDPTWMain.processOne(prob, random);
//        scheduler.updateSchedule(solution2Schedule(sol));
//        final StatisticsDTO stats =
//                Solvers.computeStats(prob.getGlobalStateObject(),
//                        scheduler.getCurrentSchedule());
//        final double cost =
//                Gendreau06ObjectiveFunction.instance().computeCost(stats);
//        System.out.println(cost);
//        // System.out.println(Joiner.on("\n").join(scheduler.getCurrentSchedule()));
//        System.out.println(stats.movedVehicles);
//        // GendreauRun.cost =
//        // Gendreau06ObjectiveFunction.instance().computeCost(stats);
//      } catch (final InterruptedException e) {
//        return;
//      } catch (final Exception e) {
//        scheduler.reportException(e);
//      }
//      scheduler.doneForNow();
//    }
//  }
//}