package solver.search;

import model.solution.Solvable;
import solver.moves.MoveFactory;

import java.util.Random;

/**
 * Created by Eline on 21/05/2015.
 */
public abstract class LocalSearch<S extends Solvable> {
    public S solvable;
    public final MoveFactory<S> moveFactory;
    public double initialCost;
    public final Random random;
//    private final Semaphore newSnapshot;

    protected LocalSearch(S solvable, MoveFactory<S> moveFactory, double initialCost, Random random) {
        this.solvable = solvable;

        this.moveFactory = moveFactory;
        this.initialCost = initialCost;
        this.random = random;
//        this.newSnapshot = newSnapshot;
    }

    public S search() throws InterruptedException {
        boolean iterationPerformed = true;
        S bestSolution = (S)solvable.copy();

        double currentCost = solvable.calculateCost();
        double bestCost = currentCost;
        //System.out.print(currentCost+" ");
//        int nonImprovement =0;
        //System.out.println(System.currentTimeMillis()+" "+endTime);
        int i=0;
        int j=0;
        while(iterationPerformed ){
            if(Thread.interrupted()){
//                System.out.println("Interrupted");
                throw new InterruptedException();
            }
            iterationPerformed = performIteration();
            i++;
            if((currentCost = solvable.calculateCost()) < bestCost){
                j++;
                bestSolution = ( S )solvable.copy();
                bestCost = currentCost;
            }
        }
        System.out.println(i+" iterations");// - "+j+" improving iterations - ratio "+(((double)j)/((double)i)));
//        System.out.println("Done");
        //System.out.println("- "+bestCost+" "+bestSolution.calculateCost()+" +");
        return bestSolution;
        //return random.nextDouble()<0.5?solvable:bestSolution;
    }

    public abstract boolean performIteration() throws InterruptedException;
}
