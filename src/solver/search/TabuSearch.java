package solver.search;

import model.solution.Solvable;
import solver.moves.Move;
import solver.moves.MoveFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;

/**
 * Implements a Steepest descent localsearch
 * @author Tony
 */
public class TabuSearch<S extends Solvable> extends LocalSearch<S> {
    final ArrayList<String> previousSolutions;

    public TabuSearch(S solvable, MoveFactory<S> moveFactory, double initialCost, Random random) {
        super(solvable, moveFactory, initialCost, random);
        previousSolutions = new ArrayList<>();

        previousSolutions.add(solvable.toString());
    }

    @Override
    public boolean performIteration() throws InterruptedException {
        List<Move<S>> moves = moveFactory.createMoves(solvable);
        TreeMap<Double, Move<S>> moveTreeMap = new TreeMap<>();

        double bestMoveCost = Double.POSITIVE_INFINITY;
        Move<S> bestMove = null;

        for(Move<S> move : moves){
            if(Thread.interrupted())throw new InterruptedException();
            //double cost = S.calculateCost();
            //move.doMove(S);
            //double newCost = S.calculateCost();
            //move.undoMove(S);
            //double deltaCost = newCost - cost;
            double deltaCost = move.evaluateMove(solvable);

            if(deltaCost<0)moveTreeMap.put(deltaCost, move);



//            if(deltaCost < bestMoveCost ){
//                bestMoveCost = deltaCost;
//                bestMove = move;
//            }

        }
        if(!moveTreeMap.isEmpty() && moveTreeMap.firstKey()<0) {
            bestMove = moveTreeMap.get(moveTreeMap.firstKey());
            bestMove.doMove(solvable);
            while(previousSolutions.contains(solvable.toString())&&!moveTreeMap.isEmpty()){
                bestMove.undoMove(solvable);
                moveTreeMap.remove(moveTreeMap.firstKey());
                bestMove = moveTreeMap.get(moveTreeMap.firstKey());
                bestMove.doMove(solvable);
            }
            //if( scheduler!=null){
            //
            //    updateSchedule(solvable);
            //}

        }
        else return false;
        return true;
    }

}