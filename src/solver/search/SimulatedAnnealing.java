package solver.search;

import model.solution.Solvable;
import org.apache.commons.math3.util.FastMath;
import solver.moves.Move;
import solver.moves.MoveFactory;

import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Implements a Steepest descent localsearch
 * @author Tony
 */
public class SimulatedAnnealing<S extends Solvable> extends LocalSearch<S> {
    public double T=8;

    public SimulatedAnnealing(S solvable, MoveFactory<S> moveFactory, double initialCost, Random random) {
        super(solvable, moveFactory, initialCost, random);

    }

    @Override
    public boolean performIteration() throws InterruptedException {
        List<Move<S>> moves = moveFactory.createMoves(solvable);
        Collections.shuffle(moves, random);

        double bestMoveCost = Double.POSITIVE_INFINITY;
        double cost = moveFactory.getCost(solvable);
        Move<S> bestMove = null;

        for(Move<S> Move : moves){
            if(Thread.interrupted())throw new InterruptedException();
            //double cost = pdptwSolution.calculateCost();
            //move.doMove(pdptwSolution);
            //double newCost = pdptwSolution.calculateCost();
            //move.undoMove(pdptwSolution);
            //double deltaCost = newCost - cost;
            double deltaCost = Move.evaluateMove(solvable);

            double acceptanceProbability = FastMath.exp(-deltaCost)/T;
            //System.out.println(deltaCost+" "+acceptanceProbability+" "+T);
            if(acceptanceProbability > random.nextDouble()){
                bestMove = Move;
                break;
            }


        }

        //System.out.println("Perform move");
        if(bestMove !=null ) {
            bestMove.doMove(solvable);
            //T = T *0.999999;
        }
        else return false;
        return true;
    }

}
