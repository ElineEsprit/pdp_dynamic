package solver.search;

import model.solution.Solvable;
import solver.moves.Move;
import solver.moves.MoveFactory;

import java.util.List;
import java.util.Random;
import java.util.TreeMap;

/**
 * Implements a Steepest descent localsearch
 * @author Tony
 */
public class SteepestDescent<S extends Solvable> extends LocalSearch<S> {

    public SteepestDescent(S solvable, MoveFactory<S> moveFactory, double initialCost, Random random) {
        super(solvable, moveFactory, initialCost, random);

    }

    @Override
    public boolean performIteration() throws InterruptedException {
        List<Move<S>> moves = moveFactory.createMoves(solvable);
        TreeMap<Double, Move<S>> moveTreeMap = new TreeMap<>();

        double bestMoveCost = Double.POSITIVE_INFINITY;
        Move<S> bestMove = null;

        for(Move<S> move : moves){
            if(Thread.interrupted())throw new InterruptedException();
            //double cost = S.calculateCost();
            //move.doMove(S);
            //double newCost = S.calculateCost();
            //move.undoMove(S);
            //double deltaCost = newCost - cost;
            double deltaCost = move.evaluateMove(solvable);

            if(deltaCost!=0 /*&& (random.nextDouble()<0.5||moves.size()<10)*/)moveTreeMap.put(deltaCost, move);



//            if(deltaCost < bestMoveCost ){
//                bestMoveCost = deltaCost;
//                bestMove = move;
//            }

        }
        if(!moveTreeMap.isEmpty()) {
            bestMove = moveTreeMap.get(moveTreeMap.firstKey());
            bestMove.doMove(solvable);
//            if(/*bestMoveCost<0 &&*/ scheduler!=null){
//
////                updateSchedule(solvable);
//            }

        }
        else{
            return false;
        }
        return true;
    }

}