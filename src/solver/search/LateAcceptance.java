package solver.search;

import model.solution.Solvable;
import solver.moves.Move;
import solver.moves.MoveFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Implements a Steepest descent localsearch
 * @author Tony
 */
public class LateAcceptance<S extends Solvable> extends LocalSearch<S> {
    final ArrayList<Double> previousCosts;

    public LateAcceptance(S solvable, MoveFactory<S> moveFactory, double initialCost, Random random, int size, double cost) {
        super(solvable, moveFactory, initialCost, random);
        previousCosts = new ArrayList<>();
//        double cost = moveFactory.getCost(solvable);

        for(int i=0;i<size;i++)previousCosts.add(cost);
    }

    @Override
    public boolean performIteration() throws InterruptedException {
        List<Move<S>> moves = moveFactory.createMoves(solvable);

        Collections.shuffle(moves, random);

        double bestMoveCost = Double.POSITIVE_INFINITY;
        double cost = moveFactory.getCost(solvable);
        Move<S> bestMove = null;

        for(Move<S> Move : moves){
            if(Thread.interrupted())throw new InterruptedException();
            //double cost = pdptwSolution.calculateCost();
            //move.doMove(pdptwSolution);
            //double newCost = pdptwSolution.calculateCost();
            //move.undoMove(pdptwSolution);
            //double deltaCost = newCost - cost;
            double deltaCost = Move.evaluateMove(solvable);

            //System.out.println(deltaCost);

            if((cost + deltaCost < previousCosts.get(0)) && deltaCost!=0 && !previousCosts.contains(cost+deltaCost) && random.nextDouble()<0.5){
                //System.out.println((cost+deltaCost-initialCost));
                bestMoveCost = deltaCost;
                bestMove = Move;
                break;
            }


        }

        //System.out.println("Perform move");
        if(bestMove !=null ) {
            bestMove.doMove(solvable);
            previousCosts.remove(0);
            previousCosts.add(cost+bestMoveCost);
        }
        else return false;
        return true;
    }


}
