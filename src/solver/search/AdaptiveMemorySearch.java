package solver.search;

import model.problem.Destination;
import model.problem.DestinationType;
import model.solution.PDPTWSolution;
import model.solution.Route2;
import org.apache.commons.math3.util.FastMath;
import solver.moves.MoveFactory;
import solver.moves.assignmentmoves.PutMoveFactory;
import solver.moves.routemoves.InsertMoveFactory;

import java.util.*;

/**
 * Created by Eline on 21/05/2015.
 */
public abstract class AdaptiveMemorySearch extends LocalSearch<PDPTWSolution> {
    private static final int MAX_AM_SIZE = 32;
    public final TreeMap<Double, PDPTWSolution> adaptiveMemory;

    protected AdaptiveMemorySearch(PDPTWSolution solvable, MoveFactory<PDPTWSolution> moveFactory, double initialCost, Random random) {
        super(solvable, moveFactory, initialCost, random);

        adaptiveMemory = new TreeMap<>();
    }

    public PDPTWSolution search() throws InterruptedException {
        boolean iterationPerformed = true;
        PDPTWSolution bestSolution = (PDPTWSolution)solvable.copy();

        double currentCost = solvable.calculateCost();
        double bestCost = currentCost;
        //System.out.print(currentCost+" ");
//        int nonImprovement =0;
        //System.out.println(System.currentTimeMillis()+" "+endTime);
        int i=0;
        int j=0;
        int nonImprovement=0;
        while(nonImprovement<10
          ){
            if(Thread.interrupted()){
//                System.out.println("Interrupted");
                throw new InterruptedException();
            }
            iterationPerformed = performIteration();
            if(!iterationPerformed){
//                if(adaptiveMemory.size()<=1)break;
//                System.out.println("Restart");
                solvable = fromAdaptiveMemory();
                nonImprovement++;
//                break;
            }
//            nonImprovement = 0;
            currentCost = solvable.calculateCost();
            if((adaptiveMemory.size()<MAX_AM_SIZE || currentCost< adaptiveMemory.lastKey())){
                putInAdaptiveMemory(solvable);
            }
            i++;
            if(currentCost< bestCost){
                j++;
                bestSolution = ( PDPTWSolution )solvable.copy();
                bestCost = currentCost;

            }
        }
        System.out.println(i+" iterations");// - "+j+" improving iterations - ratio "+(((double)j)/((double)i)));
//        System.out.println("Done");
        //System.out.println("- "+bestCost+" "+bestSolution.calculateCost()+" +");
        return bestSolution;
        //return random.nextDouble()<0.5?solvable:bestSolution;
    }

    private PDPTWSolution fromAdaptiveMemory() throws InterruptedException {
//        if(adaptiveMemory.size()==0)return solvable;
        PDPTWSolution solution = new PDPTWSolution(solvable.problem);
        solution.routes.clear();

        ArrayList<Double> keys = new ArrayList<>();
        for(Map.Entry<Double, PDPTWSolution> entry: adaptiveMemory.entrySet()){
            for(int i=0;i<entry.getValue().routes.size();i++){
                if(entry.getValue().routes.get(i).getFirstChangeAllowed()!=solvable.routes.get(i).getFirstChangeAllowed()){
                   keys.add(entry.getKey());
                }
            }
        }
        for(Double key : keys){
            adaptiveMemory.remove(key);
        }

//        if(solvable.allDestinations().size()>60){
//            for(Map.Entry<Double, PDPTWSolution> entry: adaptiveMemory.entrySet()){
//                System.out.println(entry.getValue().toString());
//                System.out.println();
//            }
//            System.out.println();
//        }

        for(int i=0;i<solvable.routes.size();i++){
            Route2 route;
            int tried = 0;
            if (adaptiveMemory.size()>0) {
                do {
                    double key = FastMath.pow(random.nextDouble(),2) * (adaptiveMemory.lastKey() - adaptiveMemory.firstKey()) + adaptiveMemory.firstKey();
                    key = (key == adaptiveMemory.firstKey())? adaptiveMemory.firstKey() : adaptiveMemory.lowerKey(key);

                    route = adaptiveMemory.get(key).routes.get(i);
                } while (solution.cannotInsert(route) && ++tried < 10);
                if (tried >= 10) {
                    route = new Route2(i, solution);
                    for(int d = 0;d < solvable.routes.get(i).getFirstChangeAllowed();d++){
                        route.add(solvable.routes.get(i).get(d));
                    }
                }
            }
            else{
                route = new Route2(i, solution);
                for(int d = 0;d < solvable.routes.get(i).getFirstChangeAllowed();d++){
                    route.add(solvable.routes.get(i).get(d));
                }
            }
            solution.routes.add(route);
        }

//        System.out.println(Joiner.on("\n").join(solution.routes));
//System.out.println("--------------");
        ArrayList<Destination> destinations = solvable.allDestinations();
//        System.out.println(Joiner.on(" ").join(destinations));
//        System.out.println("-------------");
        for(Destination destination : destinations){
            if(destination.request.id==30){
                break;
            }
        }
        for(Destination destination : destinations){

            if(!solution.containsDestination(destination)){
                if(destination.destinationType == DestinationType.Pickup){
                    SteepestDescent<PDPTWSolution> steepestDescent = new SteepestDescent<>(solution, new PutMoveFactory(destination.request), solution.calculateCost(), random);
                    steepestDescent.performIteration();
                    if(!solution.containsDestination(destination)){
                        System.out.println();
                    }
                }else{
                    int i = solution.routeOf(destination.request.pickup);
                    SteepestDescent<Route2> steepestDescent = new SteepestDescent<>(solution.routes.get(i), new InsertMoveFactory(destination), solution.routes.get(i).calculateCost(), random);
                    steepestDescent.performIteration();
                }
            }
        }
        if(Thread.interrupted())return null;
//        System.out.println(Joiner.on("\n").join(solution.routes));
//        System.out.println("-----------");
        adaptiveMemory.clear();
        for(Route2 route2 : solution.routes){
            route2.verify();
        }
        return solution;
    }

    private void putInAdaptiveMemory(PDPTWSolution solvable) {
        double cost = FastMath.round(solvable.calculateCost()*1000.0)/1000.0;
        if(!adaptiveMemory.containsKey(cost))adaptiveMemory.put(cost, (PDPTWSolution) solvable.copy());
        if(adaptiveMemory.size()>MAX_AM_SIZE)adaptiveMemory.remove(adaptiveMemory.lastKey());
    }

    public abstract boolean performIteration() throws InterruptedException;


}
