package solver;

import model.solution.PDPTWSolution;
import solver.moves.MoveFactory;
import solver.moves.globalroutemoves.AllMoveFactory;
import solver.moves.globalroutemoves.GlobalRouteMoveFactory;
import solver.search.AdaptiveMemoryLateAcceptance;
import solver.search.LocalSearch;

import java.util.ArrayList;
import java.util.Random;

public class LocalSearchSolver extends AbstractProblemSolver {
//  private final Semaphore newSnapshot;

  public LocalSearchSolver(Random random) {
    super(random);
//    this.newSnapshot = newSnapshot;
  }

  @Override
  public PDPTWSolution solve(PDPTWSolution pdptwSolution) throws InterruptedException {
    final ArrayList<MoveFactory> moveFactories =
      new ArrayList<>();

    //moveFactories.add(new SwitchRouteMoveFactory());
    ////moveFactories.add(new AllRouteMoveFactory());
    //moveFactories.add(new ReinsertMoveFactory());
    //moveFactories.add(new SwapMoveFactory());
    //moveFactories.add(new TwoOptMoveFactory());
      moveFactories.add(new AllMoveFactory());

    final double initialCost = pdptwSolution.calculateCost();
    double cost = pdptwSolution.calculateCost();
    int nonImprovement = 0;
    int size = 600;//500+random.nextInt(200);
    int n = 0;
    for (int i = 0; i < moveFactories.size(); i = (i + 1) % moveFactories.size()) {
      n++;
      System.out.println("late acceptance " + i);
      //if (moveFactories.get(i).getClass().getSuperclass().equals(AssignmentMoveFactory.class)) {
//        final LocalSearch<PDPTWSolution> localSearch = new SimulatedAnnealing<>(pdptwSolution, (AssignmentMoveFactory)moveFactories.get(i), initialCost, random, scheduler);
//        final LocalSearch<PDPTWSolution> localSearch = new LateAcceptance<>(pdptwSolution, (GlobalRouteMoveFactory)moveFactories.get(i), initialCost, random, scheduler, size, cost);
        final LocalSearch<PDPTWSolution> localSearch = new AdaptiveMemoryLateAcceptance(pdptwSolution, (GlobalRouteMoveFactory)moveFactories.get(i), initialCost, random, size, cost);

//        size = size*2;
        pdptwSolution = localSearch.search();
//      }else if(moveFactories.get(i).getClass().getSuperclass().equals(RouteMoveFactory.class)){
//        for(int j=0;j<pdptwSolution.routes.size();j++){
////          final LocalSearch<Route2> localSearch = new SimulatedAnnealing<>(pdptwSolution.routes.get(j), (RouteMoveFactory)moveFactories.get(i), initialCost, random, scheduler);
//          final LocalSearch<Route2> localSearch = new LateAcceptance<>(pdptwSolution.routes.get(j), (RouteMoveFactory)moveFactories.get(i), initialCost, random, scheduler);
//          Route2 route = localSearch.search();
//          pdptwSolution.routes.remove(j);
//          pdptwSolution.routes.add(j, route);
//          //pdptwSolution = ().pdptwSolution;
//        }
//      }
      final double newCost = pdptwSolution.calculateCost();
      System.out.println(
        "initial cost "+initialCost+" cost " + cost + " new cost " + newCost + " " + (newCost < cost));
      System.out.println(newCost+" : "+pdptwSolution.calculateTravelTime()+" "+pdptwSolution.calculateLateness()+" "+pdptwSolution.calculateOvertime());
      if (cost <= newCost) {
        nonImprovement++;
      }
      //else {
      //
      //  scheduler.updateSchedule(RtElineSolver.solution2Schedule(pdptwSolution));
      //}
      //
      //  System.out.println(Joiner.on("\n").join(scheduler.getCurrentSchedule()));
      //  //
      //  //final StatisticsDTO stats = Solvers.computeStats(solution.problem.getGlobalStateObject(),
      //  //  scheduler.getCurrentSchedule());
      //  //double cost = Gendreau06ObjectiveFunction.instance().computeCost(stats);
      //  System.out.println("----- Schedule updated");
      //}
      if (nonImprovement >= moveFactories.size()) {
        break;
      }
      cost = newCost;
      size = 600;//500+random.nextInt(200);
      //break;
    }
    System.out.println(n+" searches");
    return pdptwSolution;
  }
}
