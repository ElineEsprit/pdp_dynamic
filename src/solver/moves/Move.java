package solver.moves;

import model.solution.Solvable;

public interface Move<S extends Solvable> {

    public void doMove(S solvable);

    public void undoMove(S solvable);

    public double evaluateMove(S solvable);

    public double evaluateMove(S solvable, double maxDeltaCost);

    public boolean isAllowed();
}
