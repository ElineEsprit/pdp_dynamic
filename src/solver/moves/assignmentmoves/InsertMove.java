package solver.moves.assignmentmoves;

import model.problem.Destination;
import model.solution.PDPTWSolution;
import model.solution.Route2;

import java.util.ArrayList;

/**
 * Created by Eline on 3/06/2015.
 */
public class InsertMove extends AssignmentMove{

    public final Destination destination;
    public final Route2 route;
    public final int i;

    protected InsertMove(ArrayList<Route2> routes, Destination destination, Route2 route, int i) {
        super(routes);
        this.destination = destination;
        this.route = route;
        this.i = i;
    }


    public void doMove(PDPTWSolution pdptwSolution) {
        route.insert(i, destination);
    }

    public void undoMove(PDPTWSolution pdptwSolution) {
        route.remove(destination);
    }

    @Override
    public boolean isAllowed() {
        return (route.getFirstChangeAllowed()<=i);
    }

    public double evaluateMove(PDPTWSolution pdptwSolution){
        if(!isAllowed())return Double.POSITIVE_INFINITY;
        double cost = route.calculateCost();
        doMove(pdptwSolution);
        double newCost = route.calculateCost();
        undoMove(pdptwSolution);
        return newCost - cost;
    }

    @Override
    public double evaluateMove(PDPTWSolution solvable, double maxDeltaCost) {
        return evaluateMove(solvable);

    }

}
