package solver.moves.assignmentmoves;

import model.problem.*;
import model.solution.*;
import solver.moves.*;

import java.util.*;

/**
 * Created by Eline on 3/06/2015.
 */
public class InsertMoveFactory extends AssignmentMoveFactory{
    public final Destination destination;

    public InsertMoveFactory(Destination destination){
        this.destination = destination;
    }


    public List<Move<PDPTWSolution>> createMoves(PDPTWSolution pdptwSolution) {
        ArrayList<Move<PDPTWSolution>> assignmentMoves = new ArrayList<>();
        for(Route2 route : pdptwSolution.routes){
            if(destination.destinationType == DestinationType.Delivery && !route.containsKey(destination.request.pickup))continue;
            int ini = (destination.destinationType == DestinationType.Delivery && route.containsKey(destination.request.pickup))? route.indexOf(destination.request.pickup)+1 : 0;
            if(ini<route.getFirstChangeAllowed())ini=route.getFirstChangeAllowed();
            for (int i=ini;i<route.size()+1;i++) {
                InsertMove insertMove = new InsertMove(pdptwSolution.routes, destination, route, i);
                assignmentMoves.add(insertMove);
            }
        }
        return assignmentMoves;
    }
}
