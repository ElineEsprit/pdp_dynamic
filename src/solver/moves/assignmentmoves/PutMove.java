package solver.moves.assignmentmoves;

import model.problem.Request;
import model.solution.PDPTWSolution;
import model.solution.Route2;

import java.util.ArrayList;

public class PutMove extends AssignmentMove{

    public final Request request;
    public final Route2 route;
    public final int p, d;

    protected PutMove(ArrayList<Route2> routes, Request request, Route2 route, int p, int d) {
        super(routes);
        this.request = request;
        this.route = route;
        this.p = p;
        this.d = d;
    }


    public void doMove(PDPTWSolution pdptwSolution) {
        route.insert(d, request.delivery);
        route.insert(p, request.pickup);
    }

    public void undoMove(PDPTWSolution pdptwSolution) {
        route.remove(request.pickup);
        route.remove(request.delivery);
    }

    @Override
    public boolean isAllowed() {
        return (route.getFirstChangeAllowed()<=p);
    }

    public double evaluateMove(PDPTWSolution pdptwSolution){
        if(!isAllowed())return Double.POSITIVE_INFINITY;
        double cost = route.calculateCost();
        doMove(pdptwSolution);
        double newCost = route.calculateCost();
        undoMove(pdptwSolution);
        return newCost - cost;
    }

    @Override
    public double evaluateMove(PDPTWSolution solvable, double maxDeltaCost) {
        return evaluateMove(solvable);
    }

}
