package solver.moves.assignmentmoves;

import model.solution.*;
import solver.moves.*;

import java.util.*;

public abstract class AssignmentMove implements Move<PDPTWSolution> {
    ArrayList<Route2> routes;


    protected AssignmentMove(ArrayList<Route2> routes) {
        this.routes = routes;

    }

    public abstract void doMove(PDPTWSolution pdptwSolution);

    public abstract void undoMove(PDPTWSolution pdptwSolution);

    public double evaluateMove(PDPTWSolution pdptwSolution){
        if(!isAllowed())return Double.POSITIVE_INFINITY;
        double cost = pdptwSolution.calculateCost();
        doMove(pdptwSolution);
        double newCost = pdptwSolution.calculateCost();
        undoMove(pdptwSolution);
        return newCost - cost;
    }

}
