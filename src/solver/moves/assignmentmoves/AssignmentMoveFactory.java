package solver.moves.assignmentmoves;

import model.solution.PDPTWSolution;
import solver.moves.Move;
import solver.moves.MoveFactory;

import java.util.List;

public abstract class AssignmentMoveFactory implements MoveFactory<PDPTWSolution> {

    public abstract List<Move<PDPTWSolution>> createMoves(PDPTWSolution pdptwSolution);

    public double getCost(PDPTWSolution pdptwSolution){
        return pdptwSolution.calculateCost();
    }
}
