package solver.moves.assignmentmoves;

import model.problem.*;
import model.solution.*;
import solver.moves.*;

import java.util.*;

public class SwitchRouteMoveFactory extends AssignmentMoveFactory {

    public List<Move<PDPTWSolution>> createMoves(PDPTWSolution pdptwSolution) {
        List<Move<PDPTWSolution>> assignmentMoveList = new ArrayList<>();
        for (int i=0;i<pdptwSolution.routes.size();i++){
            for (int j=0;j<pdptwSolution.routes.size();j++) {
                if(i==j) continue;
                if(pdptwSolution.routes.get(i).size()==2 && pdptwSolution.routes.get(j).size()==0)continue;
                if(j>0 && pdptwSolution.routes.get(j-1).size()==0 && pdptwSolution.routes.get(j).size()==0)continue;

                int start = pdptwSolution.routes.get(j).getFirstChangeAllowed();

                for(Request request : pdptwSolution.routes.get(i).getRequests()){
                    for (int p = start; p<= pdptwSolution.routes.get(j).size();p++) {
                        for (int d = p; d <= pdptwSolution.routes.get(j).size();d++) {
                            assignmentMoveList.add(new SwitchRouteMove(pdptwSolution.routes, i, j, request ,p,d));
                        }
                    }
                }
            }
        }
        //System.out.println(assignmentMoveList.size());
        return assignmentMoveList;
    }
}
