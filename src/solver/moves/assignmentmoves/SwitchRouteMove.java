package solver.moves.assignmentmoves;

import model.problem.Request;
import model.solution.PDPTWSolution;
import model.solution.Route2;

import java.util.ArrayList;

public class SwitchRouteMove extends AssignmentMove {
    public Route2 route, newRoute;
    public final int pNew, dNew;
    public int p, d;
    public final Request request;
    public Route2 originalWith, newWithout, originalWithout, newWith;

    protected SwitchRouteMove(ArrayList<Route2> destinations, int route, int newRoute, Request request, int pNew, int dNew) {
        super(destinations);
        this.route = routes.get(route);
        this.newRoute = routes.get(newRoute);
        this.pNew = pNew;
        this.dNew = dNew;

        this.request = request;
    }

    public void doMove(PDPTWSolution pdptwSolution) {
        p = this.route.indexOf(request.pickup);
        route.remove(request.pickup);
        d = this.route.indexOf(request.delivery);
        route.remove(request.delivery);

        newRoute.insert(request, pNew, dNew);

    }

    public void undoMove(PDPTWSolution pdptwSolution) {
        newRoute.remove(request);
        route.insert(request, p, d);

    }

    @Override
    public boolean isAllowed() {
        p = this.route.indexOf(request.pickup);
        return (route.getFirstChangeAllowed()<=p && newRoute.getFirstChangeAllowed()<=pNew);
    }

    public double evaluateMove(PDPTWSolution pdptwSolution){
        if(!isAllowed())return Double.POSITIVE_INFINITY;
        double cost = route.calculateCost()+newRoute.calculateCost();
        doMove(pdptwSolution);
        double newCost = route.calculateCost()+newRoute.calculateCost();
        undoMove(pdptwSolution);
        return newCost - cost;
    }

    @Override
    public double evaluateMove(PDPTWSolution solvable, double maxDeltaCost) {


        return evaluateMove(solvable);
    }
}
