package solver.moves.assignmentmoves;

import model.problem.*;
import model.solution.*;
import solver.moves.*;

import java.util.*;

public class PutMoveFactory extends AssignmentMoveFactory {
    public final Request request;

    public PutMoveFactory(Request request){
        this.request = request;
    }

    public List<Move<PDPTWSolution>> createMoves(PDPTWSolution pdptwSolution) {
        ArrayList<Move<PDPTWSolution>> assignmentMoves = new ArrayList<>();
        for(int i=0;i<pdptwSolution.routes.size();i++){
            Route2 route = pdptwSolution.routes.get(i);
            int start = route.getFirstChangeAllowed();

//            System.out.println(start);
            for (int p=start;p<route.size()+1;p++) {
                for (int d=p;d<route.size()+1;d++) {
                    PutMove addMove = new PutMove(pdptwSolution.routes, request, route, p, d);
                    assignmentMoves.add(addMove);
                }
            }
        }
        return assignmentMoves;
    }
}
