package solver.moves;

import model.solution.Solvable;

import java.util.List;

public interface MoveFactory<S extends Solvable> {

    public List<Move<S>> createMoves(S solvable);

    double getCost(S solvable);
}
