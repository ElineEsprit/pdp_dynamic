package solver.moves.globalroutemoves;

import model.solution.*;
import solver.moves.*;

import java.util.*;

/**
 * Created by Eline on 21/05/2015.
 */
public abstract class GlobalRouteMoveFactory implements MoveFactory<PDPTWSolution> {

    public abstract List<Move<PDPTWSolution>> createMoves(PDPTWSolution solution);

    public double getCost(PDPTWSolution solution){
        return solution.calculateCost();
    }
}
