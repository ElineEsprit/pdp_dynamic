package solver.moves.globalroutemoves;

import model.problem.*;
import model.solution.*;

/**
 * Created by Eline on 21/05/2015.
 */
public class SwapMove extends GlobalRouteMove {

    public final Destination s1,s2;

    public SwapMove(Destination s1, Destination s2, Route2 route, PDPTWSolution solution) {
        super(solution, route);
        this.s1 = s1;
        this.s2 = s2;
    }

    @Override
    public void doMove(PDPTWSolution solution) {
        int i1 = route.indexOf(s1);
        route.remove(s1);
        int i2 = route.indexOf(s2);
        route.remove(s2);
        route.insert(i2,s1);
        route.insert(i1,s2);
        route.verify();
    }

    @Override
    public void undoMove(PDPTWSolution solution) {
        doMove(solution);
        route.verify();
    }

    @Override
    public boolean isAllowed() {
        int i1 = route.indexOf(s1);
        int i2 = route.indexOf(s2);
        return (route.getFirstChangeAllowed()<=i1 && route.getFirstChangeAllowed()<=i2 );
    }

    @Override
    public double evaluateMove(PDPTWSolution solution, double maxDeltaCost) {

        return evaluateMove(solution);
    }
}
