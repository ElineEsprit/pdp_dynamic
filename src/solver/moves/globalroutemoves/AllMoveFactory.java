package solver.moves.globalroutemoves;

import model.solution.*;
import solver.moves.*;

import java.util.*;

public class AllMoveFactory extends GlobalRouteMoveFactory {
    public List<Move<PDPTWSolution>> createMoves(PDPTWSolution solution) {
        //System.out.println(solution.calculateCost());
        ArrayList<Move<PDPTWSolution>> routeMoves = new ArrayList<>();
        routeMoves.addAll(new ReinsertMoveFactory().createMoves(solution));
        routeMoves.addAll(new SwapMoveFactory().createMoves(solution));
        routeMoves.addAll(new TwoOptMoveFactory().createMoves(solution));
        //routeMoves.addAll(new ExchangeMoveFactory().createMoves(solution));
        routeMoves.addAll(new SwitchRouteMoveFactory().createMoves(solution));
        return routeMoves;
    }
}
