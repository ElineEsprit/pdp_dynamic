package solver.moves.globalroutemoves;

import model.problem.*;
import model.solution.*;
import solver.moves.*;

import java.util.*;

public class AddMoveFactory extends GlobalRouteMoveFactory {
    public final Request request;

    public AddMoveFactory(Request request){
         this.request = request;
    }

    public List<Move<PDPTWSolution>> createMoves(PDPTWSolution solution) {
        ArrayList<Move<PDPTWSolution>> routeMoves = new ArrayList<>();
        for (Route2 route : solution.routes) {
            int start = 0;
            start = route.getFirstChangeAllowed();
            for (int p=start;p<route.size()+1;p++) {
                for (int d=p;d<route.size()+1;d++) {
                    AddMove addMove = new AddMove(request, p, d, route, solution);
                    routeMoves.add(addMove);
                }
            }
        }
        return routeMoves;
    }
}
