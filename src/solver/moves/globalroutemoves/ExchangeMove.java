package solver.moves.globalroutemoves;

import model.problem.*;
import model.solution.*;

public class ExchangeMove extends GlobalRouteMove {
    public final Route2 route1, route2;
    public final int pNew1, dNew1, pNew2, dNew2;
    public int p1, d1, p2, d2;
    public final Request request1, request2;
    public Route2 before1, before2, after1, after2;

    protected ExchangeMove(PDPTWSolution solution, Route2 route1, Route2 route2, int new1, int new11, int new2, int new21, Request request1, Request request2) {
        super(solution, route1);
        this.route1 = route1;
        this.route2 = route2;
        pNew1 = new1;
        dNew1 = new11;
        pNew2 = new2;
        dNew2 = new21;
        this.request1 = request1;
        this.request2 = request2;
    }


    public void doMove(PDPTWSolution pdptwSolution) {
        p1 = this.route1.indexOf(request1.pickup);
        route1.remove(request1.pickup);
        d1 = this.route1.indexOf(request1.delivery);
        route1.remove(request1.delivery);

        p2 = this.route2.indexOf(request2.pickup);
        route2.remove(request2.pickup);
        d2 = this.route2.indexOf(request2.delivery);
        route2.remove(request2.delivery);

        route1.insert(request2, pNew2, dNew2);
        route2.insert(request1, pNew1, dNew1);
        route1.verify();
        route2.verify();

    }

    public void undoMove(PDPTWSolution pdptwSolution) {
        route1.remove(request2);
        route2.remove(request1);
        route1.insert(request1, p1, d1);
        route2.insert(request2, p2, d2);
        route1.verify();
        route2.verify();
    }

    @Override
    public boolean isAllowed() {
        p1 = this.route.indexOf(request1.pickup);
        p2 = this.route2.indexOf(request2.pickup);
        int firstChangeAllowed1 = route1.getFirstChangeAllowed();
        int firstChangeAllowed2 = route2.getFirstChangeAllowed();
        return (firstChangeAllowed1 <=p1 && firstChangeAllowed1 <= pNew2 && firstChangeAllowed2 <= p2 && firstChangeAllowed2 <= pNew1);
    }

    public double evaluateMove(PDPTWSolution pdptwSolution){
        if(!isAllowed())return Double.POSITIVE_INFINITY;
        double cost = route1.calculateCost()+route2.calculateCost();
        doMove(pdptwSolution);
        double newCost = route1.calculateCost()+route2.calculateCost();
        undoMove(pdptwSolution);
        //double oCost = route1.calculateCost()+route2.calculateCost();
        //System.out.println(cost+" "+oCost);
        return newCost - cost;
    }

    @Override
    public double evaluateMove(PDPTWSolution solvable, double maxDeltaCost) {


        return evaluateMove(solvable);
    }
}
