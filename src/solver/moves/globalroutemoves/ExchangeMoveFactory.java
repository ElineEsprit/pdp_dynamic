package solver.moves.globalroutemoves;

import model.problem.*;
import model.solution.*;
import solver.moves.*;

import java.util.*;


public class ExchangeMoveFactory extends GlobalRouteMoveFactory {


    public List<Move<PDPTWSolution>> createMoves(PDPTWSolution pdptwSolution) {
        List<Move<PDPTWSolution>> assignmentMoveList = new ArrayList<>();
        for (int i=0;i<pdptwSolution.routes.size();i++){
            for (int j=0;j<pdptwSolution.routes.size();j++) {
                if(i==j) continue;
                if(j>0 && pdptwSolution.routes.get(j-1).size()==0 && pdptwSolution.routes.get(j).size()==0)continue;

                int start1 = pdptwSolution.routes.get(j).getFirstChangeAllowed();
                int start2 = pdptwSolution.routes.get(i).getFirstChangeAllowed();

                for(Request request1 : pdptwSolution.routes.get(i).getRequests()){
                    for (int pNew1 = start1; pNew1<= pdptwSolution.routes.get(j).size()-2;pNew1++) {
                        for (int dNew1 = pNew1; dNew1 <= pdptwSolution.routes.get(j).size()-2;dNew1++) {
                            //TODO: Remove if not working
                            if(pNew1 < pdptwSolution.routes.get(j).size() && pdptwSolution.routes.get(j).get(pNew1).timeWindow.startTime < request1.pickup.timeWindow.startTime) continue;
                            for(Request request2 : pdptwSolution.routes.get(j).getRequests()) {
                                for (int pNew2 = start2; pNew2 <= pdptwSolution.routes.get(i).size()-2; pNew2++) {
                                    for (int dNew2 = pNew2; dNew2 <= pdptwSolution.routes.get(i).size()-2; dNew2++) {
                                        //TODO: Remove if not working
                                        if (pNew2 < pdptwSolution.routes.get(i).size() && pdptwSolution.routes.get(i).get(pNew2).timeWindow.startTime < request2.pickup.timeWindow.startTime)continue;
                                        if(new Random().nextDouble()<0.1)assignmentMoveList.add(new ExchangeMove(pdptwSolution, pdptwSolution.routes.get(i), pdptwSolution.routes.get(j), pNew1, dNew1, pNew2, dNew2, request1, request2));
                                    }
                                }
                            }
                        }

                    }
                    //solver.moves.routemoves.AddMove bestMove = null;
                    //double bestCost = Double.POSITIVE_INFINITY;
                    //List<Move<Route2>> addMoves = new solver.moves.routemoves.AddMoveFactory(request).createMoves(pdptwSolution.routes.get(j));
                    //for(Move<Route2> addMove : addMoves){
                    //    if(((solver.moves.routemoves.AddMove)addMove).route.equals(pdptwSolution.routes.get(i)))continue;
                    //    double deltaCost = addMove.evaluateMove(pdptwSolution.routes.get(j));
                    //    if(deltaCost < bestCost) bestMove = (solver.moves.routemoves.AddMove)addMove;
                    //}
                    //
                    //assignmentMoveList.add(new SwitchRouteMove(pdptwSolution, i, j, request , bestMove.p, bestMove.d));
                }
            }
        }
        //System.out.println(assignmentMoveList.size());
        //System.out.println("SwitchRouteMove "+assignmentMoveList.size());
        return assignmentMoveList;
    }
}
