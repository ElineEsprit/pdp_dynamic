package solver.moves.globalroutemoves;

import model.problem.*;
import model.solution.*;
import solver.moves.*;

import java.util.*;


public class SwitchRouteMoveFactory extends GlobalRouteMoveFactory {


    public List<Move<PDPTWSolution>> createMoves(PDPTWSolution pdptwSolution) {
        List<Move<PDPTWSolution>> assignmentMoveList = new ArrayList<>();
        for (int i=0;i<pdptwSolution.routes.size();i++){
            for (int j=0;j<pdptwSolution.routes.size();j++) {
                if(i==j) continue;
                if(pdptwSolution.routes.get(i).size()==2 && pdptwSolution.routes.get(j).size()==0)continue;
                if(j>0 && pdptwSolution.routes.get(j-1).size()==0 && pdptwSolution.routes.get(j).size()==0)continue;

                int start = pdptwSolution.routes.get(j).getFirstChangeAllowed();

                for(Request request : pdptwSolution.routes.get(i).getRequests()){
                    for (int p = start; p<= pdptwSolution.routes.get(j).size();p++) {
                        for (int d = p; d <= pdptwSolution.routes.get(j).size();d++) {
                            //TODO: Remove if not working
                            if(p < pdptwSolution.routes.get(j).size() && pdptwSolution.routes.get(j).get(p).timeWindow.startTime < request.pickup.timeWindow.startTime) continue;
                            assignmentMoveList.add(new SwitchRouteMove(pdptwSolution, i, j, request ,p,d));
                        }
                    }
                    //solver.moves.routemoves.AddMove bestMove = null;
                    //double bestCost = Double.POSITIVE_INFINITY;
                    //List<Move<Route2>> addMoves = new solver.moves.routemoves.AddMoveFactory(request).createMoves(pdptwSolution.routes.get(j));
                    //for(Move<Route2> addMove : addMoves){
                    //    if(((solver.moves.routemoves.AddMove)addMove).route.equals(pdptwSolution.routes.get(i)))continue;
                    //    double deltaCost = addMove.evaluateMove(pdptwSolution.routes.get(j));
                    //    if(deltaCost < bestCost) bestMove = (solver.moves.routemoves.AddMove)addMove;
                    //}
                    //
                    //assignmentMoveList.add(new SwitchRouteMove(pdptwSolution, i, j, request , bestMove.p, bestMove.d));
                }
            }
        }
        //System.out.println(assignmentMoveList.size());
        //System.out.println("SwitchRouteMove "+assignmentMoveList.size());
        return assignmentMoveList;
    }
}
