package solver.moves.globalroutemoves;

import model.problem.*;
import model.solution.*;
import solver.moves.*;

import java.util.*;

/**
 * Created by Eline on 21/05/2015.
 */
public class SwapMoveFactory extends GlobalRouteMoveFactory {


    @Override
    public List<Move<PDPTWSolution>> createMoves(PDPTWSolution solution) {
        List<Move<PDPTWSolution>> routeMoveList =new ArrayList<>();
        for (Route2 route : solution.routes) {
            for (int i=route.getFirstChangeAllowed();i<route.size()-1;i++){
                for (int j=i+1;j<route.size();j++){
                    if(route.getKey(i).destinationType.equals(DestinationType.Pickup)&&route.indexOf(route.getKey(i).request.delivery)<=j)continue;
                    if(route.getKey(i).destinationType.equals(DestinationType.Delivery)&&route.indexOf(route.getKey(i).request.pickup)>j)continue;
                    if(route.getKey(j).destinationType.equals(DestinationType.Pickup)&&route.indexOf(route.getKey(j).request.delivery)<=i)continue;
                    if(route.getKey(j).destinationType.equals(DestinationType.Delivery)&&route.indexOf(route.getKey(j).request.pickup)>i)continue;

                    if(!route.getKey(i).request.equals(route.getKey(j).request) ){
                        //TODO: Remove if not working
                        if(route.get(j).timeWindow.startTime > route.get(i).timeWindow.startTime) continue;
                        routeMoveList.add(new SwapMove(route.getKey(i), route.getKey(j), route, solution));
                    }
                }
            }
        }
        //System.out.println("SwapMove "+routeMoveList.size());
        return routeMoveList;
    }

}
