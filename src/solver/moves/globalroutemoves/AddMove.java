package solver.moves.globalroutemoves;

import model.problem.*;
import model.solution.*;

public class AddMove extends GlobalRouteMove {

    public final Request request;
    public final int p, d;

    protected AddMove(Request request, int p, int d, Route2 route, PDPTWSolution solution) {
        super(solution, route);
        this.request = request;
        this.p = p;
        this.d = d;
    }


    public void doMove(PDPTWSolution solution) {
        route.insert(d, request.delivery);
        route.insert(p, request.pickup);
        route.verify();
    }

    public void undoMove(PDPTWSolution solution) {
        route.remove(request.pickup);
        route.remove(request.delivery);
        route.verify();
    }

    @Override
    public boolean isAllowed() {
        return (route.getFirstChangeAllowed()<=p);
    }

    @Override
    public double evaluateMove(PDPTWSolution solution, double maxDeltaCost) {
        return evaluateMove(solution);
    }



}
