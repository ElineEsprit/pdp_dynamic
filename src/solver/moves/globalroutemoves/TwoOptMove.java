package solver.moves.globalroutemoves;

import model.problem.*;
import model.solution.*;

import java.util.*;

/**
 * Created by Eline on 21/05/2015.
 */
public class TwoOptMove extends GlobalRouteMove {

    public final Destination sa, sb, sc, sd;

    public TwoOptMove(Destination sa, Destination sb, Destination sc, Destination sd, Route2 route, PDPTWSolution solution) {
        super(solution, route);
        this.sa = sa;
        this.sb = sb;
        this.sc = sc;
        this.sd = sd;
    }

    @Override
    public void doMove(PDPTWSolution solution) {
        ArrayList<Destination> temp = new ArrayList<>();
        int ib = route.indexOf(sb);
        int ic = route.indexOf(sc);
        for(int i = ic ; i>=ib ; i--){
            temp.add(0, route.getKey(i));
            route.remove(route.getKey(i));

        }
        for(Destination destination : temp){
            route.insert(ib, destination);
        }
        route.verify();
    }

    @Override
    public void undoMove(PDPTWSolution solution) {
        new TwoOptMove(sa, sc, sb, sd, route, solution).doMove(solution);
        route.verify();
        //doMove(route);
    }

    @Override
    public boolean isAllowed() {
        int ib = route.indexOf(sb);
        return (route.getFirstChangeAllowed()<=ib);
    }

    @Override
    public double evaluateMove(PDPTWSolution solution, double maxDeltaCost) {





        return evaluateMove(solution);
    }
}
