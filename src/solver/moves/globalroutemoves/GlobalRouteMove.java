package solver.moves.globalroutemoves;

import model.solution.*;
import solver.moves.*;

/**
 * Created by Eline on 21/05/2015.
 */
public abstract class GlobalRouteMove implements Move<PDPTWSolution> {
    public final Route2 route;



    protected GlobalRouteMove(PDPTWSolution solution, Route2 route) {
        this.route = route;

    }

    public abstract void doMove(PDPTWSolution solution);

    public abstract void undoMove(PDPTWSolution solution);

    public double evaluateMove(PDPTWSolution solution){
        if(!isAllowed())return Double.POSITIVE_INFINITY;
        double cost = route.calculateCost();
        doMove(solution);
        double newCost = route.calculateCost();
        undoMove(solution);
        double deltaCost = newCost - cost;
        return deltaCost;
    }

}
