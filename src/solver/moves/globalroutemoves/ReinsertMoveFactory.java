package solver.moves.globalroutemoves;

import model.problem.*;
import model.solution.*;
import solver.moves.*;

import java.util.*;

/**
 * Created by Eline on 21/05/2015.
 */
public class ReinsertMoveFactory extends GlobalRouteMoveFactory {

    @Override
    public List<Move<PDPTWSolution>> createMoves(PDPTWSolution solution) {
        List<Move<PDPTWSolution>> routeMoveList =new ArrayList<>();
        for (Route2 route : solution.routes) {
            Location previous = route.pdptwSolution.problem.depot;
            Location next = (route.isEmpty()?null:route.getKey(0));
            Location current = null;


            for (int i=0;i<route.size();i++){
                if(i < route.getFirstChangeAllowed())continue;
                //current = next;
                //next = (i<route.size()-1?route.get(i+1):route.pdptwSolution.problem.depot);
                //if(!isInCircle(previous,current,next)) {
                //Location newPrevious = route.pdptwSolution.problem.depot;
                //Location newNext = null;
                int initialJ = 0;
                int finalJ = route.size();
                if(route.getKey(i).destinationType.equals(DestinationType.Delivery))initialJ = route.indexOf(route.getKey(i).request.pickup)+1;
                if(route.getKey(i).destinationType.equals(DestinationType.Pickup))finalJ = route.indexOf(route.getKey(i).request.delivery);

                while(initialJ < route.getFirstChangeAllowed()){
                    initialJ++;
                }

                for (int j = initialJ; j < finalJ; j++) {


                    //newNext = (j < route.size() ? route.get(j) : route.pdptwSolution.problem.depot);
                    if (j!=i && i+1!=j){//&& isInCircle(newPrevious, current, newNext)) {
                        ReinsertMove reinsertMove = new ReinsertMove(route.getKey(i), route, j, solution);
                        routeMoveList.add(reinsertMove);
                    }
                    //newPrevious = newNext;
                }
                //}
                //previous = current;
            }
        }
        //System.out.println("ReinsertMove "+routeMoveList.size());
        //System.out.println(moveList.size());
        return routeMoveList;
    }
}