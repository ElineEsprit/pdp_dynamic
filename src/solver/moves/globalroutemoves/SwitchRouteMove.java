package solver.moves.globalroutemoves;

import com.google.common.base.Joiner;
import model.problem.Request;
import model.solution.PDPTWSolution;
import model.solution.Route2;

public class SwitchRouteMove extends GlobalRouteMove {

    public Route2 newRoute;
    public final int pNew, dNew;
    public int p, d;
    public final Request request;
    public Route2 originalWith, newWithout, originalWithout, newWith;

    protected SwitchRouteMove(PDPTWSolution solution, int route, int newRoute, Request request, int pNew, int dNew) {
        super(solution, solution.routes.get(route));
//        this.route = solution.routes.get(route);
        this.newRoute = solution.routes.get(newRoute);
        this.pNew = pNew;
        this.dNew = dNew;

        this.request = request;
    }

    public void doMove(PDPTWSolution pdptwSolution) {
        if(!(this.route.contains(request.pickup) && this.route.contains(request.delivery))){
//            System.out.println(AdaptiveMemoryLateAcceptance.lastMove);
            System.out.println(request.id);
            System.out.println(Joiner.on("\n").join(pdptwSolution.routes));
            System.out.println();
        }
        p = this.route.indexOf(request.pickup);
        route.remove(request.pickup);
        d = this.route.indexOf(request.delivery);

        route.remove(request.delivery);


        newRoute.insert(request, pNew, dNew);
        route.verify();
        newRoute.verify();
    }

    public void undoMove(PDPTWSolution pdptwSolution) {
        newRoute.remove(request);
        route.insert(request, p, d);
        route.verify();
        newRoute.verify();
    }

    @Override
    public boolean isAllowed() {
        p = this.route.indexOf(request.pickup);
        return (route.getFirstChangeAllowed()<=p && newRoute.getFirstChangeAllowed()<=pNew);
    }

    public double evaluateMove(PDPTWSolution pdptwSolution){
        if(!isAllowed())return Double.POSITIVE_INFINITY;
        double cost = route.calculateCost()+newRoute.calculateCost();
        doMove(pdptwSolution);
        double newCost = route.calculateCost()+newRoute.calculateCost();
        undoMove(pdptwSolution);
        return newCost - cost;
    }

    @Override
    public double evaluateMove(PDPTWSolution solvable, double maxDeltaCost) {


        return evaluateMove(solvable);
    }
}
