package solver.moves.routemoves;

import model.problem.Request;
import model.solution.Route2;
import solver.moves.Move;

import java.util.ArrayList;
import java.util.List;

public class AddMoveFactory extends RouteMoveFactory {
    public final Request request;

    public AddMoveFactory(Request request){
         this.request = request;
    }

    public List<Move<Route2>> createMoves(Route2 route) {
        ArrayList<Move<Route2>> routeMoves = new ArrayList<>();
        int start = 0;
        start = route.getFirstChangeAllowed();
        for (int p=start;p<route.size()+1;p++) {
            for (int d=p;d<route.size()+1;d++) {
                AddMove addMove = new AddMove(request, p, d, route);
                routeMoves.add(addMove);
            }
        }
        return routeMoves;
    }
}
