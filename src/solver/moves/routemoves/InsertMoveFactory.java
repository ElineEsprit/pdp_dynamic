package solver.moves.routemoves;

import model.problem.Destination;
import model.problem.DestinationType;
import model.solution.Route2;
import solver.moves.Move;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eline on 3/06/2015.
 */
public class InsertMoveFactory extends RouteMoveFactory{
    public final Destination destination;

    public InsertMoveFactory(Destination destination){
        this.destination = destination;
    }


    public List<Move<Route2>> createMoves(Route2 route) {
        ArrayList<Move<Route2>> assignmentMoves = new ArrayList<>();
        int ini = (destination.destinationType == DestinationType.Delivery && route.containsKey(destination.request.pickup))? route.indexOf(destination.request.pickup)+1 : 0;
        if(ini<route.getFirstChangeAllowed())ini=route.getFirstChangeAllowed();
        for (int i=ini;i<route.size()+1;i++) {
            InsertMove insertMove = new InsertMove(destination, route, i);
            assignmentMoves.add(insertMove);
        }
    
        return assignmentMoves;
    }
}
