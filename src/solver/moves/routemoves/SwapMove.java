package solver.moves.routemoves;

import model.problem.Destination;
import model.solution.Route2;

/**
 * Created by Eline on 21/05/2015.
 */
public class SwapMove extends RouteMove {

    public final Destination s1,s2;

    public SwapMove(Destination s1, Destination s2, Route2 route) {
        super(route);
        this.s1 = s1;
        this.s2 = s2;
    }

    @Override
    public void doMove(Route2 route) {
        int i1 = route.indexOf(s1);
        route.remove(s1);
        int i2 = route.indexOf(s2);
        route.remove(s2);
        route.insert(i2,s1);
        route.insert(i1,s2);
    }

    @Override
    public void undoMove(Route2 route) {
        doMove(route);
    }

    @Override
    public boolean isAllowed() {
        int i1 = route.indexOf(s1);
        int i2 = route.indexOf(s2);
        return (route.getFirstChangeAllowed()<=i1 && route.getFirstChangeAllowed()<=i2 );
    }

    @Override
    public double evaluateMove(Route2 solvable, double maxDeltaCost) {

        return evaluateMove(solvable);
    }
}
