package solver.moves.routemoves;

import model.solution.*;
import solver.moves.*;

/**
 * Created by Eline on 21/05/2015.
 */
public abstract class RouteMove implements Move<Route2> {
    public final Route2 route;



    protected RouteMove(Route2 route) {
        this.route = route;

    }

    public abstract void doMove(Route2 route);

    public abstract void undoMove(Route2 route);

    public double evaluateMove(Route2 route){
        if(!isAllowed())return Double.POSITIVE_INFINITY;
        double cost = route.calculateCost();
        doMove(route);
        double newCost = route.calculateCost();
        undoMove(route);
        double deltaCost = newCost - cost;
        return deltaCost;
    }

}
