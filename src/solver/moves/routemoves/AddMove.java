package solver.moves.routemoves;

import model.problem.Request;
import model.solution.Route2;

public class AddMove extends RouteMove{

    public final Request request;
    public final int p, d;

    protected AddMove(Request request, int p, int d, Route2 route) {
        super(route);
        this.request = request;
        this.p = p;
        this.d = d;
    }


    public void doMove(Route2 route) {
        route.insert(d, request.delivery);
        route.insert(p, request.pickup);
    }

    public void undoMove(Route2 route) {
        route.remove(request.pickup);
        route.remove(request.delivery);
    }

    @Override
    public boolean isAllowed() {
        return (route.getFirstChangeAllowed()<=p);
    }

    @Override
    public double evaluateMove(Route2 solvable, double maxDeltaCost) {
        return evaluateMove(solvable);
    }



}
