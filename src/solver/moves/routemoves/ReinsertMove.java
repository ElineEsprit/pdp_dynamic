package solver.moves.routemoves;

import model.problem.*;
import model.solution.*;

/**
 * Created by Eline on 21/05/2015.
 */
public class ReinsertMove extends RouteMove {
    public final Destination destination;
    public final int index, newIndex;

    public ReinsertMove(Destination destination, Route2 route, int newIndex) {
        super(route);
        this.destination = destination;
        index = route.indexOf(destination);
        this.newIndex = newIndex;
    }

    @Override
    public void doMove(Route2 route) {
        route.remove(destination);
        route.insert(newIndex, destination);
    }

    @Override
    public void undoMove(Route2 route) {
        route.remove(destination);
        route.insert(index, destination);
    }

    @Override
    public boolean isAllowed() {
        return (route.getFirstChangeAllowed()<=index && route.getFirstChangeAllowed()<=newIndex);
    }

    @Override
    public double evaluateMove(Route2 route, double maxDeltaCost) {
        //double extraRouteTime = route.extraTravelTime(destination, newIndex) + route.travelTimeReduction(destination, index);
        //
        //double extraLateness = 0;
        //int current, otherIndex;
        //double previousLeavingTime;
        //Location previousLocation = null;
        //
        //double destinationLateness = route.get(destination) - destination.timeWindow.endTime;
        //if(destinationLateness<0)destinationLateness=0;
        //extraLateness -= destinationLateness;
        //
        //if(index>newIndex){
        //    current = newIndex;
        //    otherIndex = index;
        //
        //    if(current>0){
        //        previousLeavingTime = route.get(route.getKey(current-1))+route.getKey(current-1).serviceTime;
        //        previousLocation = route.getKey(current-1);
        //    }else{
        //        previousLeavingTime = route.pdptwSolution.problem.getGlobalStateObject().getTime();
        //        previousLocation = new Location(route.pdptwSolution.problem.getGlobalStateObject().getVehicles().get(route.index).getLocation());
        //    }
        //    double newArrivalTime = previousLeavingTime + route.calculateTime(previousLocation, destination);
        //    if(newArrivalTime < destination.timeWindow.startTime)newArrivalTime = destination.timeWindow.startTime;
        //    if(newArrivalTime > destination.timeWindow.endTime) extraLateness += newArrivalTime - destination.timeWindow.endTime;
        //    previousLeavingTime = newArrivalTime + destination.serviceTime;
        //    previousLocation = destination;
        //}else{
        //    current = index;
        //    otherIndex = newIndex+1 ;
        //    if(current>0){
        //        previousLeavingTime = route.get(route.getKey(current-1))+route.getKey(current-1).serviceTime;
        //        previousLocation = route.getKey(current-1);
        //    }else{
        //        previousLeavingTime = route.pdptwSolution.problem.getGlobalStateObject().getTime();
        //        previousLocation = new Location(route.pdptwSolution.problem.getGlobalStateObject().getVehicles().get(route.index).getLocation());
        //    }
        //    current+=1;
        //
        //}
        //
        //for(int i=current;i<route.size();i++){
        //
        //    Destination currentLocation = route.getKey(i);
        //    double currentArrivalTime = previousLeavingTime + route.calculateTime(previousLocation, currentLocation);
        //
        //    //if(i==otherIndex && currentArrivalTime >= route.get(currentLocation) && extraRouteTime+extraLateness > maxDeltaCost){
        //    //    return Double.POSITIVE_INFINITY;
        //    //}
        //
        //    if(currentArrivalTime < currentLocation.timeWindow.startTime){
        //        currentArrivalTime =  currentLocation.timeWindow.startTime;
        //    }
        //    if(currentArrivalTime == route.get(currentLocation)&& i> otherIndex){
        //        break;
        //    }
        //
        //    if(currentArrivalTime > currentLocation.timeWindow.endTime) extraLateness += (currentArrivalTime - currentLocation.timeWindow.endTime) - (route.get(currentLocation)>currentLocation.timeWindow.endTime? route.get(currentLocation)-currentLocation.timeWindow.endTime : 0);
        //    previousLocation = currentLocation;
        //    previousLeavingTime = currentArrivalTime + currentLocation.serviceTime;
        //
        //    if(i+1==otherIndex && i+1 <route.size()){
        //        if(index > newIndex){
        //            i++;
        //            otherIndex++;
        //        }else{
        //            currentLocation = route.getKey(index);
        //            currentArrivalTime = previousLeavingTime + route.calculateTime(previousLocation, currentLocation);
        //            if(currentArrivalTime < currentLocation.timeWindow.startTime)currentArrivalTime = currentLocation.timeWindow.startTime;
        //            if(currentArrivalTime > currentLocation.timeWindow.endTime) extraLateness += (currentArrivalTime - currentLocation.timeWindow.endTime) - (route.get(currentLocation)>currentLocation.timeWindow.endTime? route.get(currentLocation)-currentLocation.timeWindow.endTime : 0);
        //            previousLocation = currentLocation;
        //            previousLeavingTime = currentArrivalTime + currentLocation.serviceTime;
        //
        //        }
        //    }
        //}
        //double depotArrivalTime = previousLeavingTime + route.calculateTime(previousLocation, route.pdptwSolution.problem.depot);
        //double extraOvertime =0;
        //double originalDepotTime = route.get(((Destination)previousLocation)) + ((Destination)previousLocation).serviceTime + route.calculateTime(previousLocation, route.pdptwSolution.problem.depot);
        //if(originalDepotTime > route.pdptwSolution.problem.depot.timeWindow.endTime){
        //    extraOvertime -= originalDepotTime - route.pdptwSolution.problem.depot.timeWindow.endTime;
        //}
        //if(depotArrivalTime > route.pdptwSolution.problem.depot.timeWindow.endTime){
        //    extraOvertime += depotArrivalTime - route.pdptwSolution.problem.depot.timeWindow.endTime;
        //}
        //
        double deltaCost = evaluateMove(route);
        //double customDeltaCost = (extraRouteTime+extraLateness+extraOvertime)/(60*1000);
        //double difference = (int)(deltaCost*1000) - (int)(customDeltaCost*1000);
        //if(difference != 0){
        //    System.out.println("Difference: "+index+" "+newIndex+" *** "+customDeltaCost +" "+ deltaCost);
        //}else{
        //    System.out.println("Same: "+index+" "+newIndex+" *** "+customDeltaCost);
        //}
        //if(extraRouteTime > maxDeltaCost){
        //    return Double.POSITIVE_INFINITY;
        //}
        return deltaCost;
    }



}
