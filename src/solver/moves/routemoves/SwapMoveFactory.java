package solver.moves.routemoves;

import model.problem.*;
import model.solution.Route2;
import solver.moves.Move;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eline on 21/05/2015.
 */
public class SwapMoveFactory extends RouteMoveFactory {


    @Override
    public List<Move<Route2>> createMoves(Route2 route) {
        List<Move<Route2>> routeMoveList =new ArrayList<>();
        for (int i=route.getFirstChangeAllowed();i<route.size()-1;i++){
            for (int j=i+1;j<route.size();j++){
                if(route.getKey(i).destinationType.equals(DestinationType.Pickup)&&route.indexOf(route.getKey(i).request.delivery)<j)continue;
                if(route.getKey(i).destinationType.equals(DestinationType.Delivery)&&route.indexOf(route.getKey(i).request.pickup)>i)continue;

                if(!route.getKey(i).request.equals(route.getKey(j).request) ){
                    routeMoveList.add(new SwapMove(route.getKey(i), route.getKey(j), route));
                }
            }
        }
        return routeMoveList;
    }

}
