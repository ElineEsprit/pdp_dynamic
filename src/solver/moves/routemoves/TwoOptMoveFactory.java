package solver.moves.routemoves;

import model.problem.Destination;
import model.solution.Route2;
import solver.moves.Move;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eline on 21/05/2015.
 */
public class TwoOptMoveFactory extends RouteMoveFactory {

    @Override
    public List<Move<Route2>> createMoves(Route2 route) {
        List<Move<Route2>> routeMoveList =new ArrayList<>();

        for (int j=2;j<route.size();j++){
            if(Line2D.linesIntersect(route.pdptwSolution.problem.depot.xCo,route.pdptwSolution.problem.depot.yCo,route.getKey(0).xCo,route.getKey(0).yCo,route.getKey(j - 1).xCo,route.getKey(j - 1).yCo,route.getKey(j).xCo,route.getKey(j).yCo)){
                if (twoOptAllowed(null,route.getKey(0), route.getKey(j - 1),route.getKey(j), route)) {
                    routeMoveList.add(new TwoOptMove(null,route.getKey(0), route.getKey(j - 1),route.getKey(j), route));
                }
            }
        }
        //if(Line2D.linesIntersect(route.pdptwSolution.problem.depot.xCo,route.pdptwSolution.problem.depot.yCo,route.getKey(0).xCo,route.getKey(0).yCo,route.get(route.size()-1).xCo,route.get(route.size()-1).yCo,route.pdptwSolution.problem.depot.xCo,route.pdptwSolution.problem.depot.yCo)){
        //    moveList.add(new TwoOptMove(null,route.get(0), route.get(route.size()-1),null, route));
        //}


        for (int i=1;i<route.size()-1;i++){
            for (int j=i+2;j<route.size();j++){
                if(Line2D.linesIntersect(route.getKey(i-1).xCo,route.getKey(i-1).yCo,route.getKey(i).xCo,route.getKey(i).yCo,route.getKey(j-1).xCo,route.getKey(j-1).yCo,route.getKey(j).xCo,route.getKey(j).yCo)){
                    if (twoOptAllowed(route.getKey(i - 1),route.getKey(i), route.getKey(j - 1),route.getKey(j), route)) {
                        routeMoveList.add(new TwoOptMove(route.getKey(i - 1),route.getKey(i), route.getKey(j - 1),route.getKey(j), route));
                    }
                }
            }
            if(Line2D.linesIntersect(route.getKey(i - 1).xCo, route.getKey(i - 1).yCo, route.getKey(i).xCo, route.getKey(i).yCo, route.getKey(route.size() - 1).xCo, route.getKey(route.size() - 1).yCo, route.pdptwSolution.problem.depot.xCo, route.pdptwSolution.problem.depot.yCo)){
                if (twoOptAllowed(route.getKey(i - 1),route.getKey(i), route.getKey(route.size() - 1),null, route)) {
                    routeMoveList.add(new TwoOptMove(route.getKey(i - 1),route.getKey(i), route.getKey(route.size()-1),null, route ));
                }
            }


        }
        return routeMoveList;
    }

    public boolean twoOptAllowed(Destination sa, Destination sb, Destination sc, Destination sd, Route2 route){
        if(route.indexOf(sa)<route.getFirstChangeAllowed())return false;
        int ib = route.indexOf(sb);
        int ic = route.indexOf(sc);

        for(int i=ib;i<=ic;i++){
            switch(route.getKey(i).destinationType){
                case Pickup:{
                    if(route.indexOf(route.getKey(i).request.delivery)<=ic)return false;
                }break;
                case Delivery:{
                    if(route.indexOf(route.getKey(i).request.pickup)>=ib)return false;
                }break;
                default:break;
            }
        }
        return true;
    }
}