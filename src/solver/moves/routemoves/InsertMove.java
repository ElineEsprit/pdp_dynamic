package solver.moves.routemoves;

import model.problem.Destination;
import model.solution.Route2;

/**
 * Created by Eline on 3/06/2015.
 */
public class InsertMove extends RouteMove{

    public final Destination destination;
    public final Route2 route;
    public final int i;

    protected InsertMove(Destination destination, Route2 route, int i) {
        super(route);
        this.destination = destination;
        this.route = route;
        this.i = i;
    }


    public void doMove(Route2 route) {
        route.insert(i, destination);
    }

    public void undoMove(Route2 route) {
        route.remove(destination);
    }

    @Override
    public boolean isAllowed() {
        return (route.getFirstChangeAllowed()<=i);
    }

    public double evaluateMove(Route2 route){
        if(!isAllowed())return Double.POSITIVE_INFINITY;
        double cost = route.calculateCost();
        doMove(route);
        double newCost = route.calculateCost();
        undoMove(route);
        return newCost - cost;
    }

    @Override
    public double evaluateMove(Route2 solvable, double maxDeltaCost) {
        return evaluateMove(solvable);

    }
}
