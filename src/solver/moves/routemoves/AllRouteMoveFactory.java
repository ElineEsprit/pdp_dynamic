package solver.moves.routemoves;

import model.solution.*;
import solver.moves.*;

import java.util.*;

public class AllRouteMoveFactory extends RouteMoveFactory {

    public List<Move<Route2>> createMoves(Route2 route) {
        ArrayList<Move<Route2>> routeMoves = new ArrayList<>();
        routeMoves.addAll(new ReinsertMoveFactory().createMoves(route));
        routeMoves.addAll(new SwapMoveFactory().createMoves(route));
        routeMoves.addAll(new TwoOptMoveFactory().createMoves(route));
        return routeMoves;
    }
}
