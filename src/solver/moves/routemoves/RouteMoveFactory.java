package solver.moves.routemoves;

import model.solution.Route2;
import solver.moves.Move;
import solver.moves.MoveFactory;

import java.util.List;

/**
 * Created by Eline on 21/05/2015.
 */
public abstract class RouteMoveFactory implements MoveFactory<Route2> {

    public abstract List<Move<Route2>> createMoves(Route2 route);

    public double getCost(Route2 route){
        return route.calculateCost();
    }
}
