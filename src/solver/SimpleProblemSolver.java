package solver;

import model.problem.*;
import model.solution.*;

import java.util.*;

public class SimpleProblemSolver extends AbstractProblemSolver {
  private final AbstractRouteSolver routeSolver;

  public SimpleProblemSolver(Random random, AbstractRouteSolver routeSolver) {
    super(random);
    this.routeSolver = routeSolver;
  }

  @Override
  public PDPTWSolution solve(PDPTWSolution solution) {
    final TreeMap<Double, Request> angleTreeMap = new TreeMap<>();
    // ArrayList<Destination> locations = new ArrayList<>();
    int i = 0;
    final int rand = new Random().nextInt(360);
    for (final Request request : solution.problem.getRequests()) {
      // locations.add(request.destination);
      double angle =
        angleFromCoordinate(solution.problem.depot, request) + rand;
      angle = (angle + 360) % 360;
      // System.out.println(angle);
      angleTreeMap.put(angle, request);
      i++;
      if (i == 500) {
        break;
      }
    }
    ArrayList<Request> itinerary = null;
    int numDestinations = 100;
    //
    // double nd = (double)angleTreeMap.size()/(double)numDestinations;
    // int q = (nd%1==0)?(int)nd : (int)(nd+1);
    final int q = solution.problem.numVehicles;

    final double fnd = (double) angleTreeMap.size() / q;

    numDestinations = fnd % 1 == 0 ? (int) fnd : (int) (fnd + 1);

    final Iterator<Map.Entry<Double, Request>> iterator =
      angleTreeMap.entrySet().iterator();
    // Iterator<Route2> routeIterator = solution.routes.iterator();
    int r = 0;
    while ((itinerary = getNextItinerary(iterator, fnd, q - r)) != null) {
      Route2 route = new Route2(r, solution);
      for (final Request request : itinerary) {
        route.add(request);
      }
      if (r >= solution.problem.numVehicles - 100) {
        route = routeSolver.solve(route);
      }
      System.out.println("Route solved with cost: " + route.calculateCost());
      solution.routes.remove(r);
      solution.routes.add(r, route);
      r++;
    }
    System.out.println("Attention");
    return solution;
  }

  private double angleFromCoordinate(Destination depot, Request request) {
    final double deltaXP = request.pickup.xCo - depot.xCo;
    final double deltaYP = request.pickup.yCo - depot.yCo;
    final double radP = Math.atan2(deltaYP, deltaXP); // In radians
    final double degP = radP * (180 / Math.PI);

    final double deltaXD = request.delivery.xCo - depot.xCo;
    final double deltaYD = request.delivery.yCo - depot.yCo;
    final double radD = Math.atan2(deltaYD, deltaXD); // In radians
    final double degD = radD * (180 / Math.PI);

    return (degP + degD) / 2;
  }

  private ArrayList<Request> getNextItinerary(
      Iterator<Map.Entry<Double, Request>> iterator, double numDestinations,
      int remaining) {
    if (!iterator.hasNext()) {
      return null;
    }
    final ArrayList<Request> itinerary = new ArrayList<>();
    int nd = 0;

    final int numDest = (int) numDestinations
        + (random.nextDouble() < numDestinations % 1 ? 1 : 0);
    while ((nd < numDest || remaining == 1) && iterator.hasNext()) {
      itinerary.add(iterator.next().getValue());
      nd++;
    }
    return itinerary;
  }
}
