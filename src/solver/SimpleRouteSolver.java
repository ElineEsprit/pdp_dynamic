package solver;//package solver;
//
//import java.util.ArrayList;
//
//import model.solution.Route2;
//import solver.moves.MoveFactory;
//import solver.moves.routemoves.ReinsertMoveFactory;
//import solver.moves.routemoves.SwapMoveFactory;
//import solver.moves.routemoves.TwoOptMoveFactory;
//import solver.search.LateAcceptance;
//import solver.search.LocalSearch;
//
//public class SimpleRouteSolver extends AbstractRouteSolver {
//
//    public SimpleRouteSolver(double time) {
//        super(time);
//    }
//
//    public Route2 solve(Route2 route, double endTime) {
//        ArrayList<MoveFactory<Route2>> moveFactories = new ArrayList<>();
//        int firstChangeAllowed;
//        moveFactories.add(new SwapMoveFactory(firstChangeAllowed));
//        moveFactories.add(new ReinsertMoveFactory(firstChangeAllowed));
//        moveFactories.add(new TwoOptMoveFactory(firstChangeAllowed));
//        double initialCost =  route.calculateCost();
//        double cost = initialCost;
//        int nonImprovement = 0;
//        for(int i=0;i<moveFactories.size();i=(i+1)%moveFactories.size()) {
//            LocalSearch<Route2> localSearch = new LateAcceptance<>(route, moveFactories.get(i), initialCost, endTime);
//            Route2 = localSearch.search();
//            //System.out.print(cost+" ");
//            if (cost == (cost = route.calculateCost())) {
//                //System.out.println("No improvement");
//                nonImprovement++;
//            }
//            if (nonImprovement >= moveFactories.size()) break;
//        }
//        //System.out.println("Final cost: "+cost);
//
//        return route;
//    }
//}
