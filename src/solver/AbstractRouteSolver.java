package solver;

import model.solution.*;

public abstract class AbstractRouteSolver implements Solver<Route2>{

    public abstract Route2 solve(Route2 route);
}
