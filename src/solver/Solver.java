package solver;

import model.solution.*;

public interface Solver<S extends Solvable> {

    public S solve(S solution) throws InterruptedException;
}
