package solver;

import model.solution.*;

import java.util.*;

public abstract class AbstractProblemSolver implements Solver<PDPTWSolution> {

    public final Random random;

    protected AbstractProblemSolver(Random random) {
        this.random = random;
    }

    public abstract PDPTWSolution solve(PDPTWSolution solution) throws InterruptedException;
}
