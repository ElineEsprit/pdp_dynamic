package model.solution;

import com.google.common.base.Joiner;
import model.problem.Destination;
import model.problem.DestinationType;
import model.problem.Location;
import model.problem.Request;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class Route2 extends ArrayList<Destination> implements Solvable {
    private final int index;
    public static int ALPHA = 1, BETA = 1;
    public final PDPTWSolution pdptwSolution;

    public Route2(int idx, PDPTWSolution solution) {
        index = idx;
        pdptwSolution = solution;
    }


    public void add(Request request) {
        insert(this.size(), request.pickup);
        insert(this.size(), request.delivery);
    }

    public boolean containsKey(Destination dest) {
        return this.contains(dest);
    }

    public Destination getKey(int i) {
        return this.get(i);
    }

    public Set<Request> getRequests() {
        final Set<Request> requests = new LinkedHashSet<>();
        int i=0;
        for (final Destination destination : this) {
            if(i>=getFirstChangeAllowed()){
                requests.add(destination.request);
            }
            i++;
        }
        return requests;
    }

    public void insert(Request request, int p, int d) {
        insert(d, request.delivery);
        insert(p, request.pickup);
    }

    public void insert(int index, Destination destination) {
        this.add(index, destination);
    }

    public int getFirstChangeAllowed(){
        double currentTime =0;

        Destination previous = this.pdptwSolution.problem.depot;

        int index=0;

        for(Destination current : this){
            double travelTime = Location.calculateTime(previous, current);
            currentTime += travelTime;
            if(currentTime < current.timeWindow.startTime) currentTime = current.timeWindow.startTime;
            if(currentTime - travelTime > this.pdptwSolution.problem.currentTime) return index;

            currentTime += current.serviceTime;


            previous = current;
            index++;

        }


        return index;
    }

    public ArrayList<Destination> keySet() {
        return this;
    }

    public void put(Destination delivery, double currentTime) {
        this.add(delivery);
    }

    public void remove(Request request) {
        this.remove(request.pickup);
        this.remove(request.delivery);
    }

    @Override
    public double calculateCost() {
        if(this.size() ==0) return 0;
        double cost = 0;
        double currentTime =0;

        Destination previous = this.pdptwSolution.problem.depot;

        for(Destination current : this){
            double travelTime = Location.calculateTime(previous, current);
            cost += travelTime;
            currentTime += travelTime;
            if(currentTime > current.timeWindow.endTime) cost += (currentTime - current.timeWindow.endTime)/60.0;
            if(currentTime < current.timeWindow.startTime) currentTime = current.timeWindow.startTime;

//            System.out.println(Location.calculateDistance(previous, current)+" "+travelTime);
            currentTime += current.serviceTime;

            previous = current;
        }

        Destination current = this.pdptwSolution.problem.depot;
        double travelTime = Location.calculateTime(previous, current);
        cost += travelTime;
        currentTime += travelTime;
        if(currentTime > current.timeWindow.endTime) cost += (currentTime - current.timeWindow.endTime)/60.0;

        return cost;
    }

    public double calculateTravelTime(){
        if(this.size() ==0) return 0;
        double travelTime = 0;
//        double currentTime =0;

        Destination previous = this.pdptwSolution.problem.depot;

        for(Destination current : this){
            double sectionTime = Location.calculateTime(previous, current);
            travelTime += sectionTime;

            previous = current;
        }
        double sectionTime = Location.calculateTime(previous, pdptwSolution.problem.depot);
        travelTime += sectionTime;
        return travelTime;
    }

    public double calculateLateness(){
        if(this.size() ==0) return 0;
        double lateness = 0;
        double currentTime =0;

        Destination previous = this.pdptwSolution.problem.depot;

        for(Destination current : this){
            double travelTime = Location.calculateTime(previous, current);
//            cost += travelTime;
            currentTime += travelTime;
            if(currentTime > current.timeWindow.endTime) lateness += (currentTime - current.timeWindow.endTime)/60.0;
            if(currentTime < current.timeWindow.startTime) currentTime = current.timeWindow.startTime;

//            System.out.println(Location.calculateDistance(previous, current)+" "+travelTime);
            currentTime += current.serviceTime;

            previous = current;
        }

//        Destination current = this.pdptwSolution.problem.depot;
//        double travelTime = Location.calculateTime(previous, current);
////        cost += travelTime;
//        currentTime += travelTime;
//        if(currentTime > current.timeWindow.endTime) lateness += currentTime - current.timeWindow.endTime;

        return lateness;
    }

    public double calculateOvertime(){
        if(this.size() ==0) return 0;
//        double cost = 0;
        double currentTime =0;

        Destination previous = this.pdptwSolution.problem.depot;

        for(Destination current : this){
            double travelTime = Location.calculateTime(previous, current);
//            cost += travelTime;
            currentTime += travelTime;
//            if(currentTime > current.timeWindow.endTime) cost += currentTime - current.timeWindow.endTime;
            if(currentTime < current.timeWindow.startTime) currentTime = current.timeWindow.startTime;

//            System.out.println(Location.calculateDistance(previous, current)+" "+travelTime);
            currentTime += current.serviceTime;

            previous = current;
        }

        Destination current = this.pdptwSolution.problem.depot;
        double travelTime = Location.calculateTime(previous, current);
//        cost += travelTime;
        currentTime += travelTime;
        double cost = 0;
        if(currentTime > current.timeWindow.endTime) cost += (currentTime - current.timeWindow.endTime)/60.0;

        return cost;
    }


    @Override
    public Route2 copy() {
        final Route2 route = new Route2(index, pdptwSolution);
        //route.startTime = startTime;
        //route.endTime = endTime;
        //route.travelTime = travelTime;
        for (Destination destination : this) {
            route.add(destination);
        }

        return route;
    }

    public Route2 copy(PDPTWSolution pdptwSolution) {
        final Route2 route = new Route2(index, pdptwSolution);
        for (Destination destination : this) {
            route.add(destination);
        }

        return route;
    }

    @Override
    public String toString() {
        return this.getFirstChangeAllowed()+" - "+Joiner.on(" ").join(this);
    }

    public boolean verify(){
        for(Destination destination : this){
            if(destination.destinationType.equals(DestinationType.Pickup) && !this.contains(destination.request.delivery)){
                System.out.println(Joiner.on("\n").join(pdptwSolution.routes));
                return false;
            }
            if(destination.destinationType.equals(DestinationType.Delivery) && !this.contains(destination.request.pickup)){
                System.out.println(Joiner.on("\n").join(pdptwSolution.routes));
                return false;
            }
            if(indexOf(destination.request.pickup)> indexOf(destination.request.delivery)){
                System.out.println(Joiner.on("\n").join(pdptwSolution.routes));
                return false;
            }
        }
        return true;
    }

    public String writeToFile() {
        String toFile = "";
        if(this.size() ==0) return "";
        double currentTime =0;

        Destination previous = this.pdptwSolution.problem.depot;
        double previousTime =0;

        for(Destination current : this){

            double travelTime = Location.calculateTime(previous, current);
            currentTime += travelTime;
            if(currentTime < current.timeWindow.startTime) currentTime = current.timeWindow.startTime;

            toFile = toFile + "["+previous.xCo+" "+previous.yCo+" "+previousTime+" "+(currentTime - travelTime)+"]";

//            System.out.println(Location.calculateDistance(previous, current)+" "+travelTime);
            previousTime = currentTime;
            currentTime += current.serviceTime;

            previous = current;
        }

        Destination current = this.pdptwSolution.problem.depot;
        toFile = toFile + "["+previous.xCo+" "+previous.yCo+" "+previousTime+" "+(currentTime)+"]";
        double travelTime = Location.calculateTime(previous, current);
        currentTime += travelTime;
        toFile = toFile + "["+current.xCo+" "+current.yCo+" "+currentTime+" 0]";


        return toFile;

    }
}