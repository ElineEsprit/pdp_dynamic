package model.solution;

public interface Solvable {

    Solvable copy();
    public double calculateCost();
}
