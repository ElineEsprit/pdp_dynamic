package model.solution;

import model.problem.*;

import java.util.*;

class RouteMap implements Map<Destination,Double>{
    private final ArrayList<Destination> destinations;
    private final ArrayList<Double> times;

    protected RouteMap() {
        destinations = new ArrayList<>();
        times = new ArrayList<>();
    }

    public int indexOf(Destination destination) {
        int index = destinations.indexOf(destination);
        return destinations.indexOf(destination);
    }

    protected Destination lastKey() {
        if(destinations.size()==0)return null;
        return destinations.get(destinations.size()-1);
    }

    public Destination firstKey() {
        if(destinations.size()==0)return null;
        return destinations.get(0);
    }

    public Destination nextKey(Destination previous) {
        int i = destinations.indexOf(previous);
        return (i+1>=destinations.size()? null:destinations.get(i+1));
    }

    protected Destination previousKey(Destination next) {
        int i = destinations.indexOf(next);
        return (i==0?null:destinations.get(i-1));
    }

    public int size() {
        return destinations.size();
    }

    public boolean isEmpty() {
        return destinations.isEmpty();
    }

    public boolean containsKey(Object key) {
        return destinations.contains(key);
    }

    public boolean containsValue(Object value) {
        return times.contains(value);
    }

    public Double get(Object key) {
        if(key==null)return 0.0;
        return times.get(destinations.indexOf(key));
    }

    protected Double get(int i) {
        return times.get(i);
    }

    public Destination getKey(int i) {
        return destinations.get(i);
    }

    public Double put(Destination key, Double value) {
        destinations.add(key);
        times.add(value);
        return value;
    }

    protected Double put(int index, Destination key, Double value) {
        destinations.add(index, key);
        times.add(index, value);
        return value;
    }

    public Double remove(Object key) {
        int index = destinations.indexOf(key);
        destinations.remove(key);
        return times.remove(index);
    }

    protected Double remove(int index) {
        destinations.remove(index);
        return times.remove(index);
    }

    protected Destination removeKey(int index) {
        times.remove(index);
        return destinations.remove(index);
    }

    public void putAll(Map<? extends Destination, ? extends Double> m) {
        System.out.println("Not implemented");
    }

    public void clear() {
        destinations.clear();
        times.clear();
    }

    public Set<Destination> keySet() {
        return new LinkedHashSet<>(destinations);
    }

    public Collection<Double> values() {
        return times;
    }

    public Set<Entry<Destination, Double>> entrySet() {
        LinkedHashSet<Entry<Destination, Double>> entries = new LinkedHashSet<>();
        for(int i=0;i<destinations.size();i++){
            entries.add(new AbstractMap.SimpleEntry<Destination, Double>(destinations.get(i), times.get(i)) );
        }
        return entries;
    }

    public boolean remove(Object key, Object value) {
        int index = destinations.indexOf(key);
        destinations.remove(key);
        double time = times.remove(index);
        return time == (Double)value;
    }

    public boolean replace(Destination key, Double oldValue, Double newValue) {
        int index = destinations.indexOf(key);
        double time = times.remove(index);
        times.add(index,newValue);
        return time == oldValue;
    }

    public Double replace(Destination key, Double value) {
        int index = destinations.indexOf(key);
        double time = times.remove(index);
        times.add(index,value);
        return time;
    }
}
