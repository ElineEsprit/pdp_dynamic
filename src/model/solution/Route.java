package model.solution;//package model.solution;
//
//import com.github.rinde.rinsim.central.GlobalStateObject;
//import com.github.rinde.rinsim.central.Solvers;
//import com.github.rinde.rinsim.core.model.pdp.Parcel;
//import com.github.rinde.rinsim.core.model.road.RoadModels;
//import com.github.rinde.rinsim.pdptw.common.StatisticsDTO;
//import com.github.rinde.rinsim.scenario.gendreau06.Gendreau06ObjectiveFunction;
//import com.google.common.collect.ImmutableList;
//import model.problem.Destination;
//import model.problem.Location;
//import model.problem.Request;
//
//import javax.measure.Measure;
//import java.awt.*;
//import java.util.LinkedHashSet;
//import java.util.Set;
//
//public class Route extends RouteMap implements Solvable {
//  public final int index;
//  public static int ALPHA = 1, BETA = 1;
//  public final PDPTWSolution pdptwSolution;
//  public double startTime;
//  public double endTime;
//  public double travelTime = 0;
//
//  public Route(int idx, PDPTWSolution solution) {
//    index = idx;
//    pdptwSolution = solution;
////    GlobalStateObject gso = pdptwSolution.problem.getGlobalStateObject();
////    System.out.println(gso.getSpeedUnit().toString());
//  }
//
//
//  public void add(Request request) {
//    // double currentTime =
//    // (this.isEmpty()?0:this.get(this.lastKey())+this.lastKey().serviceTime);
//    // double travelTime =
//    // calculateTime(this.isEmpty()?pdptwSolution.problem.depot:this.lastKey(),
//    // request.pickup);
//    // currentTime = currentTime + travelTime;
//    // if(currentTime<request.pickup.timeWindow.initializationTime){
//    // currentTime=request.pickup.timeWindow.initializationTime;
//    // }
//    // if(this.isEmpty())initializationTime = currentTime - travelTime;
//    // this.put(request.pickup, currentTime);
//    // currentTime += request.pickup.serviceTime;
//    // travelTime = calculateTime(request.pickup, request.delivery);
//    // currentTime = currentTime + travelTime;
//    // if(currentTime<request.delivery.timeWindow.initializationTime)currentTime=request.delivery.timeWindow.initializationTime;
//    // this.put(request.delivery, currentTime);
//    // endTime = currentTime+request.delivery.serviceTime +
//    // calculateTime(request.delivery, pdptwSolution.problem.depot);
//    insert(this.size(), request.pickup);
//    insert(this.size(), request.delivery);
//  }
//
//  public Set<Request> getRequests() {
//    final Set<Request> requests = new LinkedHashSet<>();
//    int i=0;
//    for (final Destination destination : this.keySet()) {
//      if(i>=getFirstChangeAllowed()){
//        requests.add(destination.request);
//      }
//      i++;
//    }
//    return requests;
//  }
//
//  public void insert(Request request, int p, int d) {
//    insert(d, request.delivery);
//    insert(p, request.pickup);
//  }
//
//  public double calculateTime(Location start, Location end){
//    // return time in milliseconds
//    GlobalStateObject gso = pdptwSolution.problem.getGlobalStateObject();
//    double distance = Point.distance(start.xCo, start.yCo, end.xCo, end.yCo);
//    return RoadModels.computeTravelTime(Measure.valueOf(30.0, gso.getSpeedUnit()), Measure.valueOf(distance, gso.getDistUnit()), gso.getTimeUnit());//speed = Measure<Double, Velocity>, distance = Measure<Double, Length>, outputTimeUnit = Unit<Duration>
//  }
//
//  public void insert(int index, Destination destination) {
//    Destination previous = index < this.size()
//            ? this.previousKey(this.getKey(index)) : this.lastKey();
//    Destination current = destination;
//
//    double currentTime = this.get(previous);
//
//    if (previous == null) {
//      previous = pdptwSolution.problem.depot;
//    }
//
//    currentTime += previous.serviceTime;
//    double travelTime = calculateTime(previous, current);
//    this.travelTime += travelTime;
//    currentTime = currentTime + travelTime;
//    if (currentTime < current.timeWindow.startTime) {
//      currentTime = current.timeWindow.startTime;
//    }
//    if (index == 0) {
//      startTime = currentTime - travelTime;
//    }
//
//    this.put(index, destination, currentTime);
//    Location nx = this.nextKey(destination);
//    if (nx == null) {
//      nx = pdptwSolution.problem.depot;
//    }
//    this.travelTime -= calculateTime(previous, nx);
//    previous = current;
//    current = this.nextKey(destination);
//
//    this.travelTime +=
//            current != null ? calculateTime(previous, current)
//                    : calculateTime(previous, pdptwSolution.problem.depot);
//    while (current != null) {
//
//      currentTime = this.get(previous) + previous.serviceTime;
//      travelTime = calculateTime(previous, current);
//      currentTime = currentTime + travelTime;
//      if (currentTime < current.timeWindow.startTime) {
//        currentTime = current.timeWindow.startTime;
//      }
//
//      this.replace(current, currentTime);
//
//      previous = current;
//      current = this.nextKey(previous);
//    }
//
//    endTime = this.get(this.lastKey()) + this.lastKey().serviceTime
//            + calculateTime(this.lastKey(), pdptwSolution.problem.depot);
//
//  }
//
//  public int getFirstChangeAllowed(){
//    final GlobalStateObject oneRouteObject =
//            pdptwSolution.problem.getGlobalStateObject().withSingleVehicle(index);
//
////    double currentTime = oneRouteObject.getVehicles().get(0).getDestination().isPresent()?this.get(oneRouteObject.getVehicles().get(0).getDestination().get()):0;
//
//    final ImmutableList.Builder<Parcel> routeBuilder = ImmutableList.builder();
//    int i=0;
//    for (final Destination destination : this.keySet()) {
////      if(this.get(destination)>=currentTime){
//      routeBuilder.add(destination.request.getParcel());
//      i++;
////      }
//    }
//    ImmutableList<Parcel> rt = routeBuilder.build();
//
//    int placesToVisit = rt.size() + (oneRouteObject.getVehicles().get(0).getDestination().isPresent()?-1:0);
////    System.out.println(this.size()-placesToVisit);
//
//    return this.size() - placesToVisit;
//
//  }
//
//  public void remove(Request request) {
//    this.remove(request.pickup);
//    this.remove(request.delivery);
//  }
//
//  public void remove(Destination destination) {
//    Destination previous = this.previousKey(destination);
//    if (previous == null) {
//      previous = pdptwSolution.problem.depot;
//    }
//    Destination current = this.nextKey(destination);
//    this.travelTime -= calculateTime(previous, destination);
//    this.travelTime -=
//            current != null ? calculateTime(destination, current)
//                    : calculateTime(destination, pdptwSolution.problem.depot);
//    this.travelTime +=
//            current != null ? calculateTime(previous, current)
//                    : calculateTime(previous, pdptwSolution.problem.depot);
//    do {
//      double currentTime =
//              previous.equals(pdptwSolution.problem.depot) ? 0 : this.get(previous);
//
//      currentTime += previous.serviceTime;
//      if (current == null) {
//        break;
//      }
//      final double travelTime = calculateTime(previous, current);
//
//      currentTime = currentTime + travelTime;
//      if (currentTime < current.timeWindow.startTime) {
//        currentTime = current.timeWindow.startTime;
//      }
//      if (previous.equals(pdptwSolution.problem.depot)) {
//        startTime = currentTime - travelTime;
//      }
//
//      this.replace(current, currentTime);
//
//      previous = current;
//      current = this.nextKey(previous);
//    } while (current != null);
//
//    if (this.getKey(0).equals(destination)) {
//      // initializationTime =kljb;
//    }
//
//    this.remove(destination, this.get(destination));
//
//    if (this.isEmpty()) {
//      startTime = 0;
//      endTime = 0;
//    } else {
//      endTime = this.get(this.lastKey()) + this.lastKey().serviceTime
//              + calculateTime(this.lastKey(), pdptwSolution.problem.depot);
//    }
//  }
//
////  private void printSchedule(){
////    final ImmutableList.Builder<ImmutableList<Parcel>> scheduleBuilder =
////            ImmutableList.builder();
////
////    for (final Route2 r : pdptwSolution.routes) {
////      final ImmutableList.Builder<Parcel> routeBuilder =
////              ImmutableList.builder();
////      for (final Destination dest : r.keySet()) {
////        routeBuilder.add(RtElineSolver.dest2Parcel(dest));
////      }
////      scheduleBuilder.add(routeBuilder.build());
////    }
//////    System.out.println(Joiner.on("\n").join(sol.routes));
////    final ImmutableList<ImmutableList<Parcel>> schedule =
////            scheduleBuilder.build();
////    System.out.println(Joiner.on("\n").join(schedule));
////  }
//
//  @Override
//  public double calculateCost() {
//    if (this.isEmpty()) {
//      return 0;
//    }
//
//    final GlobalStateObject oneRouteObject =
//            pdptwSolution.problem.getGlobalStateObject().withSingleVehicle(index);
//
////    double currentTime = oneRouteObject.getVehicles().get(0).getDestination().isPresent()?this.get(oneRouteObject.getVehicles().get(0).getDestination().get()):0;
//
//    final ImmutableList.Builder<Parcel> routeBuilder = ImmutableList.builder();
//    for (final Destination destination : this.keySet()) {
////      if(this.get(destination)>=currentTime){
//      routeBuilder.add(destination.request.getParcel());
////      }
//    }
//    if(oneRouteObject.getVehicles().get(0).getDestination().isPresent()){
//      if(!routeBuilder.build().contains(oneRouteObject.getVehicles().get(0).getDestination().get())){
////        printSchedule();
//        System.out.println(oneRouteObject.getVehicles().get(0).getDestination().get());
//        System.out.println(oneRouteObject.getVehicles().get(0).getContents().size());
//
//
//      }
//    }
//    final StatisticsDTO stats = Solvers.computeStats(oneRouteObject,
//            ImmutableList.of(routeBuilder.build()));
//
//    return Gendreau06ObjectiveFunction.instance().computeCost(stats);
//
//    // double lateness = 0;
//    // for (final Entry<Destination, Double> entry : this.entrySet()) {
//    // lateness += entry.getValue() - entry.getKey().timeWindow.endTime > 0
//    // ? entry.getValue() - entry.getKey().timeWindow.endTime : 0;
//    // }
//    // final double overTime =
//    // endTime - pdptwSolution.problem.depot.timeWindow.endTime > 0
//    // ? endTime - pdptwSolution.problem.depot.timeWindow.endTime : 0;
//    // final double cost = travelTime + ALPHA * lateness + BETA * overTime;
//
//  }
//
//  public double extraTravelTime(Destination destination, int i){
//    Destination previous = (i!=0)? this.getKey(i - 1) : this.pdptwSolution.problem.depot;
//    Destination next = (i!=this.size())? this.getKey(i) : this.pdptwSolution.problem.depot;
//    return this.calculateTime(previous, destination) + this.calculateTime(destination, next) - this.calculateTime(previous, next);
//  }
//
//  public double travelTimeReduction(Destination destination, int i){
//    Destination previous = (i!=0)? this.getKey(i - 1) : this.pdptwSolution.problem.depot;
//    Destination next = (i<this.size()-1)? this.getKey(i+1) : this.pdptwSolution.problem.depot;
//    return this.calculateTime(previous, next) - this.calculateTime(previous, destination) - this.calculateTime(destination, next);
//
//  }
//
//  public double replaceTravelTime(Destination destination, int i){
//    Destination previous = (i!=0)? this.getKey(i - 1) : this.pdptwSolution.problem.depot;
//    Destination next = (i<this.size()-1)? this.getKey(i+1) : this.pdptwSolution.problem.depot;
//    return this.calculateTime(previous, destination) + this.calculateTime(destination, next) - (this.calculateTime(previous, this.getKey(i)) + this.calculateTime(this.getKey(i), next));
//  }
//
//
//  // public void drawRoute(Group group) {
//  // Double[] co = new Double[this.size()*2+2];
//  //
//  // int i=0;
//  // for(Destination destination : this.keySet()){
//  // co[i*2]= PDPTWVisualizer.guiX(destination.xCo);
//  // co[i*2+1]= PDPTWVisualizer.guiY(destination.yCo);
//  // Circle circle = new Circle(PDPTWVisualizer.guiX(destination.xCo),
//  // PDPTWVisualizer.guiY(destination.yCo),1.5);
//  // group.getChildren().add(circle);
//  // i++;
//  // }
//  // co[co.length-2] = PDPTWVisualizer.guiX(pdptwSolution.problem.depot.xCo);
//  // co[co.length-1] = PDPTWVisualizer.guiY(pdptwSolution.problem.depot.yCo);
//  // Polygon polygon = new Polygon();
//  // polygon.getPoints().addAll(co);
//  // polygon.setStroke(Color.BLACK);
//  // polygon.setStrokeWidth(1);
//  // Random random = new Random();
//  // polygon.setFill(Color.color(random.nextDouble(),random.nextDouble(),random.nextDouble(),0.1));
//  // group.getChildren().add(polygon);
//  // }
//
//  @Override
//   public Route copy() {
//    final Route route = new Route(index, pdptwSolution);
//    route.startTime = startTime;
//    route.endTime = endTime;
//    route.travelTime = travelTime;
//    for (final Entry<Destination, Double> entry : this.entrySet()) {
//      route.put(entry.getKey(), entry.getValue());
//    }
//
//    return route;
//  }
//
//  public Route copy(PDPTWSolution pdptwSolution) {
//    final Route route = new Route(index, pdptwSolution);
//    route.startTime = startTime;
//    route.endTime = endTime;
//    route.travelTime = travelTime;
//    for (final Entry<Destination, Double> entry : this.entrySet()) {
//      route.put(entry.getKey(), entry.getValue());
//    }
//
//    return route;
//  }
//
//  public double getTravelTime() {
//    return travelTime;
//  }
//
//  public double getEndTime() {
//    return endTime;
//  }
//
//  public double getTardiness() {
//    double lateness = 0;
//    for (final Entry<Destination, Double> entry : this.entrySet()) {
//      lateness += entry.getValue() - entry.getKey().timeWindow.endTime > 0
//              ? entry.getValue() - entry.getKey().timeWindow.endTime : 0;
//    }
//    return lateness;
//  }
//
//  //
//  // public double calculateCost(){
//  // if(this.isEmpty())return 0;
//  // Route2 route = this.copy();
//  //
//  // Destination current = route.lastKey();
//  // Destination next = current;
//  // while((current=route.previousKey(current))!=null){
//  // double minTime = current.serviceTime + calculateTime(current,
//  // next);
//  // double latestTime = route.get(next) - minTime;
//  // if(latestTime > current.timeWindow.endTime) latestTime =
//  // current.timeWindow.endTime;
//  // route.replace(current, latestTime);
//  // next = current;
//  // }
//  // route.initializationTime = route.get(route.firstKey()) -
//  // calculateTime(route.pdptwSolution.problem.depot,
//  // route.firstKey());
//  //
//  // return route.calculateRouteCost();
//  // }
//
//  @Override
//  public String toString() {
//    return String.format("Route{%s}", keySet());
//  }
//
//}