package model.solution;

import com.google.common.base.Joiner;
import model.problem.Destination;
import model.problem.PDPTWProblem;

import java.util.ArrayList;

public class PDPTWSolution implements Solvable {
  public final PDPTWProblem problem;
  public final ArrayList<Route2> routes;

  public PDPTWSolution(PDPTWProblem problem) {
    this.problem = problem;

    routes = new ArrayList<Route2>();

    for (int i = 0; i < problem.numVehicles; i++) {
      routes.add(new Route2(i, this));
    }
  }


  private PDPTWSolution(PDPTWSolution pdptwSolution) {
    this.problem = pdptwSolution.problem;
    routes = new ArrayList<Route2>();
    for (int i = 0; i < pdptwSolution.routes.size(); i++) {
      routes.add(pdptwSolution.routes.get(i).copy(this));
    }
  }


  @Override
  public Solvable copy() {
    return new PDPTWSolution(this);
  }

  @Override
  public double calculateCost() {
    double cost =0;
    for(Route2 route : routes){
      cost += route.calculateCost();
    }
    return cost;
  }

  public String toString(){
    return Joiner.on("\n").join(this.routes);
  }

  public boolean equals(PDPTWSolution pdptwSolution){
    if(routes.size()!=pdptwSolution.routes.size())return false;
    for(int i=0;i<routes.size();i++){
      if(routes.get(i).size()!=pdptwSolution.routes.get(i).size())return false;
      for(int j=0;j<routes.get(i).size();j++){
        if(!routes.get(i).getKey(j).request.equals(pdptwSolution.routes.get(i).getKey(j).request))return false;
      }
    }
    return true;
  }

  public boolean cannotInsert(Route2 route) {
    for(Destination destination : route){
      if(this.containsDestination(destination))return true;
    }


    return false;
  }

  public boolean containsDestination(Destination destination) {
    for(Route2 route : routes){
      for(Destination dest : route){
        if(dest.equals(destination))return true;
      }
    }

    return false;
  }

  public ArrayList<Destination> allDestinations(){
    ArrayList<Destination> destinations = new ArrayList<Destination>();
    for(Route2 route : routes) {
      for (Destination dest : route) {
        destinations.add(dest);
      }
    }
    return destinations;
  }

  public int routeOf(Destination pickup) {
    for(int i=0;i<routes.size();i++){
      if(routes.get(i).contains(pickup))return i;
    }
    return -1;
  }

  public double calculateTravelTime() {
    double travelTime =0;
    for(Route2 route : routes){
      travelTime += route.calculateTravelTime();
    }
    return travelTime;
  }

  public double calculateLateness() {
    double lateness =0;
    for(Route2 route : routes){
      lateness += route.calculateLateness();
    }
    return lateness;
  }

  public double calculateOvertime() {
    double overtime =0;
    for(Route2 route : routes){
      overtime += route.calculateOvertime();
    }
    return overtime;
  }

  public String writeToFile() {
    String toFile = "";

    for(Route2 route2 : routes){
      toFile = toFile + route2.toString()+"\n";
    }

    toFile = toFile+calculateCost()+" "+calculateTravelTime()+" "+calculateLateness()+" "+calculateOvertime();
    return toFile;
  }
}
