package model.problem;

public class Request {
    public final int id;
  public final double arrivalTime;
  public final Destination pickup, delivery;
  public final PDPTWProblem problem;

  public Request(int id, double arrivalTime, double pickupX, double pickupY,
      double pickupStart, double pickupEnd, double pickupDuration,
      double deliveryX, double deliveryY, double deliveryStart,
      double deliveryEnd, double deliveryDuration, PDPTWProblem problem) {

      this.id = id;
    this.arrivalTime = arrivalTime;
    this.pickup = new Destination(pickupX, pickupY, this,
        DestinationType.Pickup, pickupStart, pickupEnd, pickupDuration);
    this.delivery = new Destination(deliveryX, deliveryY, this,
        DestinationType.Delivery, deliveryStart, deliveryEnd, deliveryDuration);
    this.problem = problem;
  }

}
