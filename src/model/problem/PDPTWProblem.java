package model.problem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class PDPTWProblem {
  private final ArrayList<Request> requests;
  public final Destination depot;
  public final int numVehicles;
  public double currentTime;

  public PDPTWProblem(String path) throws IOException {
    currentTime = 0;
    String[] fileName = path.split("_");
    File file = new File(path);
    double totalTime = ((fileName[3].equals("240"))?240:450)*60;
    depot = new Destination(2.0,2.5, null, DestinationType.Depot, 0, totalTime,0);
    numVehicles = ((fileName[3].equals("240"))?10:20);

    requests = new ArrayList<>();
    final BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
    String line = bufferedReader.readLine();
    while ((line = bufferedReader.readLine()) != null) {
      String[] args = line.split(" ");

      double arrivalTime = Double.parseDouble(args[0]);
      double pickupX = Double.parseDouble(args[2]);
      double pickupY = Double.parseDouble(args[3]);
      double pickupStart = Double.parseDouble(args[4]);
      double pickupEnd = Double.parseDouble(args[5]);
      double pickupDuration = Double.parseDouble(args[1]);
      double deliveryX = Double.parseDouble(args[7]);
      double deliveryY = Double.parseDouble(args[8]);
      double deliveryStart = Double.parseDouble(args[9]);
      double deliveryEnd = Double.parseDouble(args[10]);
      double deliveryDuration = Double.parseDouble(args[6]);

      Request request = new Request(requests.size(), arrivalTime, pickupX, pickupY,
      pickupStart, pickupEnd, pickupDuration,
      deliveryX, deliveryY, deliveryStart,
      deliveryEnd, deliveryDuration, this);
      if(arrivalTime>0)requests.add(request);
    }

    Collections.sort(requests, new Comparator<Request>() {
      @Override
      public int compare(Request o1, Request o2) {
        if (o1.arrivalTime < o2.arrivalTime) return -1;
        if (o1.arrivalTime == o2.arrivalTime) return 0;
        return 1;
      }
    });

//    System.out.println();

  }

  /**
   * @return A set containing all requests (both scheduled and unscheduled).
   */
  public ArrayList<Request> getRequests() {
    return requests;
  }

  /**
   * @return A set containing all requests which are not yet scheduled.
   */


}
