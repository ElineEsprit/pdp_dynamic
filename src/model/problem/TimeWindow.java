package model.problem;

public class TimeWindow {
    public final double startTime, endTime;

    public TimeWindow(double startTime, double endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
