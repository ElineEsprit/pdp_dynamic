package model.problem;

public class Destination extends Location {
  public final Request request;
  public final DestinationType destinationType;
  public final TimeWindow timeWindow;
  public final double serviceTime;

  public Destination(double xCo, double yCo, Request request,
      DestinationType type, double startTime, double endTime,
      double serviceTime) {
    super(xCo, yCo);
    this.request = request;
    destinationType = type;
    timeWindow = new TimeWindow(startTime, endTime);
    this.serviceTime = serviceTime;
  }

  @Override
  public String toString() {
    return String.format("%s{%s}", request.id, destinationType);
  }

}
