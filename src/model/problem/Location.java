package model.problem;

import org.apache.commons.math3.util.FastMath;

public class Location {
  public static double SPEED = 0.5 /*/ (60.0*//**1000.0*//*)*/;
//  public static LinkedHashMap<Location, LinkedHashMap<Location, Double>> lookup =
//    new LinkedHashMap<>();
  public final double xCo, yCo;

  public Location(double xCo, double yCo) {
    this.xCo = xCo;
    this.yCo = yCo;
  }

  public static double calculateDistance(Location a, Location b){
    double distance =  FastMath.sqrt(FastMath.pow(a.xCo - b.xCo, 2) + FastMath.pow(a.yCo - b.yCo, 2));
    return distance;
  }

  public static double calculateTime(Location a, Location b){
    return calculateDistance(a,b) / SPEED;
  }

}
